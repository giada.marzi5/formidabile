<?php

use App\Http\Controllers\PostsController;
use App\Models\Categorie;
use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\File;
use Spatie\YamlFrontMatter\YamlFrontMatter;
use Symfony\Component\Yaml\Yaml;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/home');
//Route::get('/', 'HomeController@index')->name('home');

//login
Route::get('/login', function () {

    return view('Login');
});

//Link pagina Home del sito
Route::get('/home', function () {

    return view('Home');
});

//Link pagina Chi Siamo del sito
Route::get('/chi-siamo', function () {

    return view('Chi-Siamo');
});


//Link pagina Risorse e sottopagine del sito
Route::get('/risorse', function () {

    return view('Risorse');
});

Route::get('/risorse-associazioni', function () {

    return view('RisorseAssociazioni');
});

Route::get('/risorse-didattiche', function () {

    return view('RisorseDidattiche');
});

 Route::get('/risorse-documentazione', [PostsController::class, 'index'])->name('documentazione');

 Route::get('posts/{post:slug}', [PostsController::class, 'show']);



// Route::get('categories/{category:slug}', function (Category $category) {

//     return view('RisorseDocumentazione', [
//         'posts' => $category->posts
//     ]);
// })->name('category');

// Route::get('authors/{author}', function (User $author) {

//     return view('RisorseDocumentazione', [
//         'posts' => $author->posts
//     ]);

// });



//Link pagina Collabora con noi 
Route::get('/collabora-con-noi', function () {
    return view('CollaboraConNoi');
});

//Link pagina Collabora con noi 
Route::get('/carica-informazioni', function () {

    return view('CollaboraCaricaInformazioni');
});

//Link pagina Collabora con noi 
Route::get('/contatti', function () {

    return view('Contatti');
});