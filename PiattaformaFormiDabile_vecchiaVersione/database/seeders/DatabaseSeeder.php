<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //questo comando crea 10 utenti differenti (casuali)
        // \App\Models\User::factory(10)->create();



        //questo ti permette di non ricevere il messaggio di errore in caso come nella categoria ci siano dei 
        //campi unique che non può creare altre categorie uguali.
        // User::truncate();
        // Category::truncate();
        // Post::truncate();

        $user = User::factory()->create([
            'name' => 'John Doe'
        ]);

        Post::factory(5)->create([
            'user_id' => $user->id
        ]);


        // //mi crea le categorie automaticamente per evitare che doppia le info, mettere unique nome e slug per le categorie
        // $user = User::factory()->create();

        // $disIntellettive = Category::create([
        //     'name' => 'Disabilità intellettive',
        //     'slug' => 'disabilità intellettive'
        // ]);

        // $disComunicazione = Category::create([
        //     'name' => 'Disturbi della comunicazione',
        //     'slug' => 'disturbi della comunicazione'
        // ]);

        // $disSpettroAutismo = Category::create([
        //     'name' => 'Disturbi dello spettro dell autismo',
        //     'slug' => 'disturbi dello spettro dell autismo'
        // ]);

        // Post::create([
        //     'user_id' => $user->id,
        //     'category_id' => $disIntellettive->id,
        //     'title' => 'Discalculia',
        //     'slug' => 'discalculia',
        //     'excerpt' => 'La discalculia, ovvero la difficoltà di apprendimento del sistema dei numeri.',
        //     'body' => '<p>La discalculia, ovvero la difficoltà di apprendimento del sistema dei numeri e dei calcoli, si presenta spesso in associazione alla dislessia ma, in alcuni più isolati casi, anche singolarmente.</p>'
        // ]);

        // Post::create([
        //     'user_id' => $user->id,
        //     'category_id' => $disComunicazione->id,
        //     'title' => 'Prova',
        //     'slug' => 'prova',
        //     'excerpt' => 'Questo è un post di prova sul disturbo della comunicazione.',
        //     'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>'
        // ]);


        // Post::create([
        //     'user_id' => $user->id,
        //     'category_id' => $disSpettroAutismo->id,
        //     'title' => 'Prova2',
        //     'slug' => 'prova2',
        //     'excerpt' => 'Questo è un post di prova sul disturbo dello spettro dell autismo.',
        //     'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> '
        // ]);
    }
}
