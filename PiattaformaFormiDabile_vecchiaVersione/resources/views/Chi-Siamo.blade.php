<!DOCTYPE html>
<html style="font-size: 16px;">

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta charset="utf-8">
  <meta name="keywords" content="Progetto FormidAbile">
  <meta name="description" content="">
  <title>Chi Siamo</title>
  <link rel="stylesheet" href="{{url('/css/nicepage.css')}}" media="screen">
  <link rel="stylesheet" href="{{url('/css/Chi-Siamo.css')}}" media="screen">
  <script class="u-script" type="text/javascript" src="{{url('/js/jquery.js')}}" "="" defer=""></script>
    <script class=" u-script" type="text/javascript" src="{{url('/js/nicepage.js')}}" "="" defer=""></script>
  <meta name="generator" content="Nicepage 4.18.5, nicepage.com">
  <link id = "u-theme-google-font"
    rel = "stylesheet"
    href = "{{url('/css/fonts.css')}}" >
      <link id = "u-page-google-font"
    rel = "stylesheet"
    href = "{{url('/css/Chi-Siamo-fonts.css')}}" >


      <script type = "application/ld+json" > {
        "@context": "http://schema.org",
        "@type": "Organization",
        "name": "Piattaforma FormidAbile",
        "logo": "images/Immagine21.png",
        "sameAs": []
      }
  </script>
  <meta name="theme-color" content="#9b74ec">
  <meta property="og:title" content="Chi-Siamo">
  <meta property="og:description" content="">
  <meta property="og:type" content="website">
</head>

<body class="u-body u-show-back-to-top u-xl-mode" data-style="blank" data-posts="" data-global-section-properties="{&quot;colorings&quot;:{&quot;light&quot;:[&quot;clean&quot;],&quot;colored&quot;:[&quot;clean&quot;],&quot;dark&quot;:[&quot;dark&quot;]}}" data-source="" data-lang="it" data-page-sections-style="[]" data-page-coloring-types="{&quot;light&quot;:[&quot;clean&quot;],&quot;colored&quot;:[&quot;clean&quot;],&quot;dark&quot;:[&quot;dark&quot;]}" data-page-category="&quot;Basic&quot;">

  <header class="u-clearfix u-header u-sticky u-white" id="sec-f943" data-animation-name="" data-animation-duration="0" data-animation-delay="0" data-animation-direction="">
    <div class="u-clearfix u-sheet u-valign-middle u-sheet-1">
      <a href="/home" data-page-id="236022295" class="u-image u-logo u-image-1" data-image-width="1920" data-image-height="1725" title="Home">
        <img src="images/Immagine21.png" class="u-logo-image u-logo-image-1">
      </a>
      <a class="u-login u-text-body-color u-text-hover-custom-color-28 u-login-1" href="/login" title="Login" target="_blank">Login</a>
      <nav class="u-menu u-menu-one-level u-offcanvas u-offcanvas-shift u-menu-1">
        <div class="menu-collapse" style="font-size: 1.125rem; letter-spacing: 0px; text-transform: uppercase; font-weight: 500;">
          <a class="u-button-style u-custom-active-border-color u-custom-active-color u-custom-border u-custom-border-color u-custom-borders u-custom-hover-border-color u-custom-hover-color u-custom-left-right-menu-spacing u-custom-padding-bottom u-custom-text-active-color u-custom-text-color u-custom-text-decoration u-custom-text-hover-color u-custom-top-bottom-menu-spacing u-nav-link" href="#" style="font-size: calc(1em + 16px); padding: 8px 18px; background-image: none;">
            <svg class="u-svg-link" viewBox="0 0 24 24">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-7d29"></use>
            </svg>
            <svg class="u-svg-content" version="1.1" id="svg-7d29" viewBox="0 0 16 16" x="0px" y="0px" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
              <g>
                <rect y="1" width="16" height="2"></rect>
                <rect y="7" width="16" height="2"></rect>
                <rect y="13" width="16" height="2"></rect>
              </g>
            </svg>
          </a>
        </div>
        <div class="u-custom-menu u-nav-container">
          <ul class="u-nav u-spacing-0 u-unstyled u-nav-1">
            <li class="u-nav-item"><a class="u-border-active-palette-1-base u-border-hover-palette-1-light-1 u-border-no-left u-border-no-right u-border-no-top u-button-style u-nav-link u-text-active-custom-color-27 u-text-hover-custom-color-28" href="/home" style="padding: 2px 20px;">Home</a>
            </li>
            <li class="u-nav-item"><a class="u-border-active-palette-1-base u-border-hover-palette-1-light-1 u-border-no-left u-border-no-right u-border-no-top u-button-style u-nav-link u-text-active-custom-color-27 u-text-hover-custom-color-28" href="/chi-siamo" style="padding: 2px 20px;">Chi Siamo</a>
            </li>
            <li class="u-nav-item"><a class="u-border-active-palette-1-base u-border-hover-palette-1-light-1 u-border-no-left u-border-no-right u-border-no-top u-button-style u-nav-link u-text-active-custom-color-27 u-text-hover-custom-color-28" href="/risorse" style="padding: 2px 20px;">Risorse</a>
            </li>
            <li class="u-nav-item"><a class="u-border-active-palette-1-base u-border-hover-palette-1-light-1 u-border-no-left u-border-no-right u-border-no-top u-button-style u-nav-link u-text-active-custom-color-27 u-text-hover-custom-color-28" href="/collabora-con-noi" style="padding: 2px 20px;">Collabora con noi</a>
            </li>
            <li class="u-nav-item"><a class="u-border-active-palette-1-base u-border-hover-palette-1-light-1 u-border-no-left u-border-no-right u-border-no-top u-button-style u-nav-link u-text-active-custom-color-27 u-text-hover-custom-color-28" href="/contatti" style="padding: 2px 20px;">Contatti</a>
            </li>
            <li class="u-nav-item"><a class="u-border-active-palette-1-base u-border-hover-palette-1-light-1 u-border-no-left u-border-no-right u-border-no-top u-button-style u-nav-link u-text-active-custom-color-27 u-text-hover-custom-color-28" style="padding: 2px 20px;">Prezzi</a>
            </li>
          </ul>
        </div>
        <div class="u-custom-menu u-nav-container-collapse">
          <div class="u-black u-container-style u-inner-container-layout u-opacity u-opacity-95 u-sidenav">
            <div class="u-inner-container-layout u-sidenav-overflow">
              <div class="u-menu-close"></div>
              <ul class="u-align-center u-nav u-popupmenu-items u-unstyled u-nav-2">
                <li class="u-nav-item"><a class="u-button-style u-nav-link" href="/home">Home</a>
                </li>
                <li class="u-nav-item"><a class="u-button-style u-nav-link" href="/chi-siamo">Chi Siamo</a>
                </li>
                <li class="u-nav-item"><a class="u-button-style u-nav-link" href="/risorse">Risorse</a>
                </li>
                <li class="u-nav-item"><a class="u-button-style u-nav-link" href="/collabora-con-noi">Collabora con noi</a>
                </li>
                <li class="u-nav-item"><a class="u-button-style u-nav-link" href="/contatti">Contatti</a>
                </li>
                <li class="u-nav-item"><a class="u-button-style u-nav-link">Prezzi</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="u-black u-menu-overlay u-opacity u-opacity-70"></div>
        </div>
      </nav>
    </div>
  </header>




  <section class="u-clearfix u-grey-5 u-block-f3fd-1" custom-posts-hash="[]" data-style="blank" data-section-properties="{&quot;margin&quot;:&quot;none&quot;,&quot;stretch&quot;:true}" id="carousel_d870" data-source="functional_fix" data-id="f3fd">
    <div class="u-clearfix u-sheet u-valign-middle-lg u-valign-middle-xl u-block-f3fd-2">
      <p class="u-text u-text-default-md u-text-default-sm u-text-default-xs u-block-f3fd-4">​La piattaforma FormidAbile nasce dalla necessità da parte di insegnanti, educatori e formatori di co​mprendere meglio le specificità e le difficoltà degli alunni con bisogni educativi speciali ed avere accesso ad informazioni affidabili riguardanti le diverse disabilità.<br><br>Quando si naviga sul web alla ricerca di informazioni concernenti disabilità, deficit cognitivi, motori o psichici, è difficile trovare informazioni utili e discriminare quelle utili poiché ce ne sono molte e talvolta sono discordanti. Inoltre, non è sempre facile discriminare tra fonti attendibili e non.<br><br>L'applicazione For​miDabile aiuta a trovare informazioni ufficiali, consigli utili e strumenti didattici che consentono a chi opera nel settore della formazione di progettare una didattica inclusiva.<br><br>Tramite il sistema di ricerca è possibile ottenere in poco tempo una serie di informazioni e risorse (es. documentazione, materiale multimediale, contatti con le associaazioni) che gli insegnanti possono usare per la formazione di persone con difficoltà di apprendimento, disabilità fisiche o psichiche.</p>
    </div>
    <style data-mode="XL" data-visited="true">
      @media (min-width: 1200px) {
        .u-block-f3fd-1 {
          background-image: none;
        }

        .u-block-f3fd-2 {
          min-height: 641px;
        }

        .u-block-f3fd-4 {
          line-height: 2;
          font-size: 1.25rem;
          margin-top: 41px;
          margin-right: auto;
          margin-bottom: 41px;
          margin-left: 0;
        }
      }
    </style>
    <style data-mode="LG">
      @media (max-width: 1199px) and (min-width: 992px) {
        .u-block-f3fd-1 {
          background-image: none;
        }

        .u-block-f3fd-2 {
          min-height: 641px;
        }

        .u-block-f3fd-4 {
          line-height: 2;
          font-size: 1.25rem;
          margin-top: 41px;
          margin-right: auto;
          margin-bottom: 41px;
          margin-left: 0;
        }
      }
    </style>
    <style data-mode="MD" data-visited="true">
      @media (max-width: 991px) and (min-width: 768px) {
        .u-block-f3fd-1 {
          background-image: none;
        }

        .u-block-f3fd-2 {
          min-height: 730px;
        }

        .u-block-f3fd-4 {
          line-height: 2;
          font-size: 1.125rem;
          margin-top: 41px;
          margin-right: auto;
          margin-bottom: 41px;
          margin-left: 0;
        }
      }
    </style>
    <style data-mode="SM">
      @media (max-width: 767px) and (min-width: 576px) {
        .u-block-f3fd-1 {
          background-image: none;
        }

        .u-block-f3fd-2 {
          min-height: 730px;
        }

        .u-block-f3fd-4 {
          line-height: 2;
          font-size: 1.125rem;
          margin-top: 41px;
          margin-right: auto;
          margin-bottom: 41px;
          margin-left: 0;
        }
      }
    </style>
    <style data-mode="XS" data-visited="true">
      @media (max-width: 575px) {
        .u-block-f3fd-1 {
          background-image: none;
        }

        .u-block-f3fd-2 {
          min-height: 730px;
        }

        .u-block-f3fd-4 {
          line-height: 2;
          font-size: 1rem;
          margin-top: 41px;
          margin-right: auto;
          margin-bottom: 41px;
          margin-left: 0;
        }
      }
    </style>
  </section>
  <section class="u-clearfix u-white u-block-5f93-1" custom-posts-hash="[[I,T],[I,T]]" data-post-id="238574804;238574804" data-section-properties="{&quot;stretch&quot;:true}" data-id="5f93" data-posts-content="[{'images':[[575,315]],'maps':[],'videos':[],'icons':[],'textWidth':565,'textHeight':305,'id':1,'groupId':2,'headingProp':'h2','textProp':'text'},{'images':[[575,315]],'maps':[],'videos':[],'icons':[],'textWidth':565,'textHeight':305,'id':1,'groupId':2,'headingProp':'h2','textProp':'text'}]" data-style="squares-2x2" id="sec-cdc2" data-source="Sketch">
    <div class="u-clearfix u-sheet u-valign-middle-lg u-valign-middle-md u-valign-middle-xl u-valign-middle-xs u-block-5f93-2" data-height-sm="1580">
      <div class="u-clearfix u-expanded-width u-gutter-12 u-layout-wrap u-block-5f93-3">
        <div class="u-gutter-0 u-layout">
          <div class="u-layout-col">
            <div class="u-size-30">
              <div class="u-layout-row">
                <div class="u-align-left u-container-style u-layout-cell u-size-30-lg u-size-30-xl u-size-32-md u-size-32-sm u-size-32-xs u-block-5f93-6">
                  <div class="u-container-layout u-block-5f93-7">

                    <p class="u-text u-text-default u-block-5f93-13"> Il progetto è nato durate l’Hacj4Learning,
                      hackhation organizzato dalla Scuola Universitaria Federale per la Formazione
                      Porfessionale in agosto 2021.<br><br>La squadra, formata da Chiara Antonietti,
                      psicologa ricercatrice, Sheila Cappelletti, formatrice, e Guya Neuroni,
                      apprendista OSA, ha lavorato allo sviluppo di un’idea progettuale che potesse
                      rispondere ad alcune criticità riscontrate nella personale esperienza
                      professionale.<br></p>

                    <p class="u-text u-text-default-md u-text-default-sm u-text-default-xl u-text-default-xs u-block-5f93-22">​L’obiettivo del progetto è fornire uno strumento utile a
                      insegnati, formatori, educatori per progettare attività didattiche ed educative
                      inclusive, personalizzate sulla base delle differenze individuali dei singoli. </p>
                  </div>
                </div>
                <div class="u-container-style u-image u-image-contain u-layout-cell u-size-28-md u-size-28-sm u-size-28-xs u-size-30-lg u-size-30-xl u-block-5f93-18" data-image-width="305" data-image-height="171">
                  <div class="u-container-layout u-valign-middle u-block-5f93-19"></div>
                </div>
              </div>
            </div>
            <div class="u-size-30">
              <div class="u-layout-row">
                <div class="u-container-style u-image u-image-contain u-image-default u-layout-cell u-size-30 u-block-5f93-10" data-image-width="431" data-image-height="179">
                  <div class="u-container-layout u-valign-middle u-block-5f93-11"></div>
                </div>
                <div class="u-container-style u-layout-cell u-size-30 u-block-5f93-20">
                  <div class="u-container-layout u-valign-top-md u-valign-top-sm u-valign-top-xs u-block-5f93-21">
                    <p class="u-text u-text-default u-block-5f93-15"> Il progetto FormidAbile è
                      stato valutato da una giuria di esperti che ha conferito al team il primo
                      premio di 5'000 CHF. <br><br>Nei mesi
                      successivi, grazie alla somma di denaro vinta, è stato possibile sviluppare un
                      prototipo della piattaforma web. <br></p>
                    <p class="u-text u-text-default u-block-5f93-23"> Giada Marzi, stagista della Scuola di
                      Informatica e di Gestione di Bellinzona ha realizzato la prima versione della
                      piattaforma FormidAbile.<br><br>Ad agosto 2022 FormidAbile è stato selezionato tra piu
                      di 100 start-up per partecipare a BoldBrain, percorso di accellerazione per
                      start-up early stage promosso dalla Fondazione AGIRE.
                      Il progetto si sta
                      concretizzando e il team si sta ampliando! </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>





    <style data-mode="XL" data-visited="true">
      @media (min-width: 1200px) {
        .u-block-5f93-2 {
          min-height: 940px;
        }

        .u-block-5f93-3 {
          margin-top: 17px;
          margin-bottom: 17px;
        }

        .u-block-5f93-6 {
          background-image: none;
          min-height: 474px;
        }

        .u-block-5f93-7 {
          padding-top: 10px;
          padding-bottom: 10px;
          padding-left: 0;
          padding-right: 0;
        }

        .u-block-5f93-13 {
          font-size: 1.125rem;
          margin-top: 32px;
          margin-right: auto;
          margin-bottom: 0;
          margin-left: 0;
        }

        .u-block-5f93-22 {
          font-size: 1.125rem;
          margin-top: 36px;
          margin-right: auto;
          margin-bottom: 0;
          margin-left: 0;
        }

        .u-block-5f93-18 {
          min-height: 474px;
          background-image: url("/images/Immagine2.png");
          background-position: 15.24% 50.43%;
        }

        .u-block-5f93-19 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 30px;
          padding-right: 30px;
        }

        .u-block-5f93-10 {
          background-image: url("/images/Immagine22.png");
          background-position: 50% 42.07%;
          min-height: 443px;
        }

        .u-block-5f93-11 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 30px;
          padding-right: 30px;
        }

        .u-block-5f93-20 {
          background-image: none;
          min-height: 442px;
        }

        .u-block-5f93-21 {
          padding-top: 7px;
          padding-bottom: 7px;
          padding-left: 0;
          padding-right: 0;
        }

        .u-block-5f93-15 {
          font-size: 1.125rem;
          margin-top: 0;
          margin-right: auto;
          margin-bottom: 0;
          margin-left: 0;
        }

        .u-block-5f93-23 {
          font-size: 1.125rem;
          margin-top: 11px;
          margin-right: auto;
          margin-bottom: 0;
          margin-left: 0;
        }
      }
    </style>
    <style data-mode="LG" data-visited="true">
      @media (max-width: 1199px) and (min-width: 992px) {
        .u-block-5f93-2 {
          min-height: 944px;
        }

        .u-block-5f93-3 {
          margin-top: 18px;
          margin-bottom: 18px;
        }

        .u-block-5f93-6 {
          background-image: none;
          min-height: 438px;
        }

        .u-block-5f93-7 {
          padding-top: 10px;
          padding-bottom: 10px;
          padding-left: 0;
          padding-right: 0;
        }

        .u-block-5f93-13 {
          font-size: 1.125rem;
          margin-top: 27px;
          margin-right: auto;
          margin-bottom: 0;
          margin-left: 0;
        }

        .u-block-5f93-22 {
          font-size: 1.125rem;
          margin-top: 11px;
          margin-right: auto;
          margin-bottom: 0;
          margin-left: 0;
        }

        .u-block-5f93-18 {
          background-image: url("/images/Immagine2.png");
          min-height: 439px;
          background-position: 15.24% 21.09%;
        }

        .u-block-5f93-19 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 30px;
          padding-right: 30px;
        }

        .u-block-5f93-10 {
          background-image: url("/images/Immagine22.png");
          background-position: 50% 8.66%;
          min-height: 475px;
        }

        .u-block-5f93-11 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 30px;
          padding-right: 30px;
        }

        .u-block-5f93-20 {
          background-image: none;
          min-height: 475px;
        }

        .u-block-5f93-21 {
          padding-top: 19px;
          padding-bottom: 19px;
          padding-left: 0;
          padding-right: 0;
        }

        .u-block-5f93-15 {
          font-size: 1.125rem;
          margin-top: 14px;
          margin-right: auto;
          margin-bottom: 0;
          margin-left: 0;
        }

        .u-block-5f93-23 {
          font-size: 1.125rem;
          margin-top: 22px;
          margin-right: auto;
          margin-bottom: 0;
          margin-left: -476px;
        }
      }
    </style>
    <style data-mode="MD" data-visited="true">
      @media (max-width: 991px) and (min-width: 768px) {
        .u-block-5f93-2 {
          min-height: 893px;
        }

        .u-block-5f93-3 {
          margin-top: 30px;
          margin-bottom: 30px;
        }

        .u-block-5f93-6 {
          background-image: none;
          min-height: 440px;
        }

        .u-block-5f93-7 {
          padding-top: 10px;
          padding-bottom: 10px;
          padding-left: 0;
          padding-right: 0;
        }

        .u-block-5f93-13 {
          font-size: 1rem;
          margin-top: 4px;
          margin-right: auto;
          margin-bottom: 0;
          margin-left: 0;
        }

        .u-block-5f93-22 {
          font-size: 1rem;
          margin-top: 25px;
          margin-right: auto;
          margin-bottom: 0;
          margin-left: 4px;
        }

        .u-block-5f93-18 {
          background-image: url("/images/Immagine2.png");
          min-height: 440px;
          background-position: 15.24% 29.33%;
        }

        .u-block-5f93-19 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 30px;
          padding-right: 30px;
        }

        .u-block-5f93-10 {
          background-image: url("/images/Immagine22.png");
          background-position: 50% 2.23%;
          min-height: 405px;
        }

        .u-block-5f93-11 {
          padding-top: 30px;
          padding-left: 30px;
          padding-right: 30px;
          padding-bottom: 25px;
        }

        .u-block-5f93-20 {
          background-image: none;
          min-height: 422px;
        }

        .u-block-5f93-21 {
          padding-top: 0;
          padding-bottom: 0;
          padding-left: 0;
          padding-right: 0;
        }

        .u-block-5f93-15 {
          font-size: 1rem;
          margin-top: 0;
          margin-right: auto;
          margin-bottom: 0;
          margin-left: 0;
        }

        .u-block-5f93-23 {
          font-size: 1rem;
          margin-top: 29px;
          margin-right: auto;
          margin-bottom: 0;
          margin-left: -362px;
        }
      }
    </style>
    <style data-mode="SM" data-visited="true">
      @media (max-width: 767px) and (min-width: 576px) {
        .u-block-5f93-2 {
          min-height: 1392px;
        }

        .u-block-5f93-3 {
          margin-top: 30px;
          margin-bottom: 20px;
        }

        .u-block-5f93-6 {
          background-image: none;
          order: 1;
          min-height: 373px;
        }

        .u-block-5f93-7 {
          padding-top: 10px;
          padding-bottom: 10px;
          padding-left: 0;
          padding-right: 0;
        }

        .u-block-5f93-13 {
          font-size: 1rem;
          margin-top: 9px;
          margin-right: auto;
          margin-bottom: 0;
          margin-left: 0;
        }

        .u-block-5f93-22 {
          font-size: 1rem;
          margin-top: 15px;
          margin-right: auto;
          margin-bottom: 0;
          margin-left: 7px;
        }

        .u-block-5f93-18 {
          background-image: url("/images/Immagine2.png");
          min-height: 344px;
          background-position: 15.24% 29.33%;
          order: 0;
        }

        .u-block-5f93-19 {
          padding-top: 30px;
          padding-left: 10px;
          padding-right: 10px;
          padding-bottom: 18px;
        }

        .u-block-5f93-10 {
          background-image: url("/images/Immagine22.png");
          background-position: 50% 2.23%;
          min-height: 259px;
        }

        .u-block-5f93-11 {
          padding-top: 30px;
          padding-left: 10px;
          padding-right: 10px;
          padding-bottom: 23px;
        }

        .u-block-5f93-20 {
          background-image: none;
          min-height: 378px;
        }

        .u-block-5f93-21 {
          padding-top: 0;
          padding-bottom: 0;
          padding-left: 0;
          padding-right: 0;
        }

        .u-block-5f93-15 {
          font-size: 1rem;
          margin-top: 0;
          margin-right: auto;
          margin-bottom: 0;
          margin-left: 9px;
        }

        .u-block-5f93-23 {
          font-size: 1rem;
          margin-top: 24px;
          margin-right: auto;
          margin-bottom: 0;
          margin-left: 9px;
        }
      }
    </style>
    <style data-mode="XS" data-visited="true">
      @media (max-width: 575px) {
        .u-block-5f93-2 {
          min-height: 1482px;
        }

        .u-block-5f93-3 {
          margin-top: 30px;
          margin-bottom: 30px;
        }

        .u-block-5f93-6 {
          background-image: none;
          order: 1;
          min-height: 509px;
        }

        .u-block-5f93-7 {
          padding-top: 10px;
          padding-bottom: 10px;
          padding-left: 0;
          padding-right: 0;
        }

        .u-block-5f93-13 {
          font-size: 1rem;
          margin-top: 9px;
          margin-right: auto;
          margin-bottom: 0;
          margin-left: 12px;
        }

        .u-block-5f93-22 {
          font-size: 1rem;
          margin-top: 15px;
          margin-right: auto;
          margin-bottom: 0;
          margin-left: 12px;
        }

        .u-block-5f93-18 {
          background-image: url("/images/Immagine2.png");
          min-height: 206px;
          background-position: 15.24% 29.33%;
          order: 0;
        }

        .u-block-5f93-19 {
          padding-top: 30px;
          padding-bottom: 18px;
          padding-left: 10px;
          padding-right: 10px;
        }

        .u-block-5f93-10 {
          background-image: url("/images/Immagine22.png");
          background-position: 50% 100%;
          min-height: 159px;
        }

        .u-block-5f93-11 {
          padding-top: 30px;
          padding-bottom: 23px;
          padding-left: 10px;
          padding-right: 10px;
        }

        .u-block-5f93-20 {
          background-image: none;
          min-height: 537px;
        }

        .u-block-5f93-21 {
          padding-top: 0;
          padding-bottom: 0;
          padding-left: 0;
          padding-right: 0;
        }

        .u-block-5f93-15 {
          font-size: 1rem;
          margin-top: 25px;
          margin-right: auto;
          margin-bottom: 0;
          margin-left: 12px;
        }

        .u-block-5f93-23 {
          font-size: 1rem;
          margin-top: 28px;
          margin-right: auto;
          margin-bottom: 0;
          margin-left: 12px;
        }
      }
    </style>
  </section>
  <section class="u-align-center u-clearfix u-grey-10 u-block-1f24-1" custom-posts-hash="[]" data-style="blank.hflip" data-section-properties="{&quot;margin&quot;:&quot;none&quot;,&quot;stretch&quot;:true}" id="carousel_1042" data-source="functional_fix" data-id="1f24" data-height-sm="244" data-shared-images-mapping="{&quot;/images/vvv.jpg&quot;:&quot;//images01.nicepagecdn.com/c461c07a441a5d220e8feb1a/24521655aa4950de9fb7413b/vvv.jpg&quot;,&quot;/images/gffgfgg-min.jpg&quot;:&quot;//images01.nicepagecdn.com/c461c07a441a5d220e8feb1a/e6899c93c62b54628cc121a5/gffgfgg-min.jpg&quot;,&quot;/images/vbvbvb.jpg&quot;:&quot;//images01.nicepagecdn.com/c461c07a441a5d220e8feb1a/21458cad19d450e2ab2ba3ef/vbvbvb.jpg&quot;,&quot;/images/dfdf.jpg&quot;:&quot;//images01.nicepagecdn.com/c461c07a441a5d220e8feb1a/3baf2d104af357e196e35dd2/dfdf.jpg&quot;}">

    <div class="u-clearfix u-sheet u-valign-middle u-block-1f24-57">
      <h2 class="u-custom-font u-font-montserrat u-text u-text-default u-block-1f24-16">Il Nostro Team</h2>
      <div class="u-expanded-width u-list u-block-1f24-28">
        <div class="u-repeater u-block-1f24-29">
          <div class="u-align-center u-container-style u-list-item u-opacity u-opacity-55 u-repeater-item u-shape-rectangle u-white u-block-1f24-30">
            <div class="u-container-layout u-similar-container u-block-1f24-31">
              <div alt="" class="u-align-center-lg u-align-center-md u-align-center-sm u-align-center-xs u-border-9 u-border-white u-image u-image-circle u-block-1f24-32" data-image-width="150" data-image-height="150"></div>
              <h4 class="u-custom-font u-text u-text-font u-block-1f24-33">Chiara Antonietti</h4>
              <p class="u-text u-text-grey-50 u-block-1f24-34">Psicologa e ricercatrice SUFFP</p>
            </div>
          </div>
          <div class="u-align-center u-container-style u-list-item u-opacity u-opacity-55 u-repeater-item u-shape-rectangle u-white u-block-1f24-36">
            <div class="u-container-layout u-similar-container u-block-1f24-37">
              <div alt="" class="u-align-center-lg u-align-center-md u-align-center-sm u-align-center-xs u-border-9 u-border-white u-image u-image-circle u-block-1f24-38" data-image-width="150" data-image-height="150"></div>
              <h4 class="u-custom-font u-text u-text-font u-block-1f24-39">Guya Neuroni</h4>
              <p class="u-text u-text-grey-50 u-block-1f24-40">Apprendista OSA handicap</p>
            </div>
          </div>
          <div class="u-align-center u-container-style u-list-item u-opacity u-opacity-55 u-repeater-item u-shape-rectangle u-white u-block-1f24-42">
            <div class="u-container-layout u-similar-container u-block-1f24-43">
              <div alt="" class="u-align-center-lg u-align-center-md u-align-center-sm u-align-center-xs u-border-9 u-border-white u-image u-image-circle u-block-1f24-44" data-image-width="150" data-image-height="148"></div>
              <h4 class="u-custom-font u-text u-text-font u-block-1f24-45">Sheila Cappelletti</h4>
              <p class="u-text u-text-grey-50 u-block-1f24-46">Formatrice professionista, inclusione andicap Ticino</p>
            </div>
          </div>
          <div class="u-align-center u-container-style u-list-item u-opacity u-opacity-55 u-repeater-item u-shape-rectangle u-white u-block-1f24-48">
            <div class="u-container-layout u-similar-container u-block-1f24-49">
              <div alt="" class="u-align-center-lg u-align-center-md u-align-center-sm u-align-center-xs u-border-9 u-border-white u-image u-image-circle u-block-1f24-50" data-image-width="1440" data-image-height="1920"></div>
              <h4 class="u-custom-font u-text u-text-font u-block-1f24-51">Giada Marzi</h4>
              <p class="u-text u-text-grey-50 u-block-1f24-52">Stager, Scuola informatica di gestione (SSSE-SIG)</p>
            </div>
          </div>
          <div class="u-align-center u-container-style u-list-item u-opacity u-opacity-55 u-repeater-item u-shape-rectangle u-white u-block-1f24-58">
            <div class="u-container-layout u-similar-container u-block-1f24-59">
              <div alt="" class="u-align-center-lg u-align-center-md u-align-center-sm u-align-center-xs u-border-9 u-border-white u-image u-image-circle u-block-1f24-60" data-image-width="2048" data-image-height="1630"></div>
              <h4 class="u-custom-font u-text u-text-font u-block-1f24-61">Dijeton Ismajli</h4>
              <p class="u-text u-text-grey-50 u-block-1f24-62">Informatico di gestione, dipl. SSS</p>
            </div>
          </div>
          <div class="u-align-center u-container-style u-list-item u-opacity u-opacity-55 u-repeater-item u-shape-rectangle u-white u-block-1f24-64">
            <div class="u-container-layout u-similar-container u-block-1f24-65">
              <div alt="" class="u-align-center-lg u-align-center-md u-align-center-sm u-align-center-xs u-border-9 u-border-white u-image u-image-circle u-block-1f24-66" data-image-width="1280" data-image-height="1280"></div>
              <h4 class="u-custom-font u-text u-text-font u-block-1f24-67">Sanju Maharjan</h4>
              <p class="u-text u-text-grey-50 u-block-1f24-68">Master in Business Administration</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <style data-mode="XL" data-visited="true">
      @media (min-width: 1200px) {
        .u-block-1f24-1 {
          background-image: none;
        }

        .u-block-1f24-57 {
          min-height: 1246px;
        }

        .u-block-1f24-16 {
          font-weight: 700;
          font-size: 3rem;
          margin-top: 47px;
          margin-left: auto;
          margin-right: auto;
          margin-bottom: 0;
        }

        .u-block-1f24-28 {
          margin-top: 47px;
          margin-bottom: 47px;
        }

        .u-block-1f24-29 {
          grid-template-columns: calc(33.3333% - 58.6667px) calc(33.3333% - 58.6667px) calc(33.3333% - 58.6667px);
          min-height: 1054px;
          grid-gap: 88px 88px;
          grid-auto-columns: calc(33.3333% - 58.6667px);
        }

        .u-block-1f24-31 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 30px;
          padding-right: 30px;
        }

        .u-block-1f24-32 {
          height: 210px;
          width: 210px;
          background-image: url("/images/ChiaraAntonietti.jpg");
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-33 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          font-weight: 700;
          margin-top: 32px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-34 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-37 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 30px;
          padding-right: 30px;
        }

        .u-block-1f24-38 {
          height: 210px;
          width: 210px;
          background-image: url("/images/GuyaNeuroni.jpg");
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-39 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          font-weight: 700;
          margin-top: 32px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-40 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-43 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 30px;
          padding-right: 30px;
        }

        .u-block-1f24-44 {
          height: 210px;
          width: 210px;
          background-image: url("/images/SheilaCappelletti.jpg");
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-45 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          font-weight: 700;
          margin-top: 32px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-46 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-49 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 30px;
          padding-right: 30px;
        }

        .u-block-1f24-50 {
          height: 210px;
          width: 210px;
          background-image: url("/images/GiadaMarzi.jpeg");
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-51 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          font-weight: 700;
          margin-top: 32px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-52 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-59 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 30px;
          padding-right: 30px;
        }

        .u-block-1f24-60 {
          height: 210px;
          width: 210px;
          background-image: url("/images/DijetonIsmajli.jpeg");
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-61 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          font-weight: 700;
          margin-top: 32px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-62 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-65 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 30px;
          padding-right: 30px;
        }

        .u-block-1f24-66 {
          height: 210px;
          width: 210px;
          background-image: url("/images/SanjuMaharjan.jpeg");
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-67 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          font-weight: 700;
          margin-top: 32px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-68 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }
      }
    </style>
    <style data-mode="LG" data-visited="true">
      @media (max-width: 1199px) and (min-width: 992px) {
        .u-block-1f24-1 {
          background-image: none;
        }

        .u-block-1f24-57 {
          min-height: 1075px;
        }

        .u-block-1f24-16 {
          font-weight: 700;
          font-size: 3rem;
          width: auto;
          margin-top: 42px;
          margin-left: auto;
          margin-right: auto;
          margin-bottom: 0;
        }

        .u-block-1f24-28 {
          margin-top: 41px;
          margin-bottom: 42px;
        }

        .u-block-1f24-29 {
          grid-gap: 88px 88px;
          grid-auto-columns: calc(33.333333333333336% - 58.6667px);
          grid-template-columns: repeat(3, calc(33.333333333333336% - 58.6667px));
          min-height: 853px;
        }

        .u-block-1f24-31 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 16px;
          padding-right: 16px;
        }

        .u-block-1f24-32 {
          background-image: url("np://user.desktop.nicepage.com/Site_71051839/images/thumbnails/ChiaraAntonietti.jpg?rand=62ce");
          height: 199px;
          width: 199px;
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-33 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          margin-top: 32px;
          margin-left: 4px;
          margin-right: 4px;
          margin-bottom: 0;
          font-weight: 700;
        }

        .u-block-1f24-34 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 4px;
          margin-right: 4px;
          margin-bottom: 0;
        }

        .u-block-1f24-37 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 16px;
          padding-right: 16px;
        }

        .u-block-1f24-38 {
          background-image: url("np://user.desktop.nicepage.com/Site_71051839/images/thumbnails/GuyaNeuroni.jpg?rand=f03a");
          height: 199px;
          width: 199px;
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-39 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          margin-top: 32px;
          margin-left: 4px;
          margin-right: 4px;
          margin-bottom: 0;
          font-weight: 700;
        }

        .u-block-1f24-40 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 4px;
          margin-right: 4px;
          margin-bottom: 0;
        }

        .u-block-1f24-43 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 16px;
          padding-right: 16px;
        }

        .u-block-1f24-44 {
          background-image: url("np://user.desktop.nicepage.com/Site_71051839/images/thumbnails/SheilaCappelletti.jpg?rand=4afa");
          height: 199px;
          width: 199px;
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-45 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          margin-top: 32px;
          margin-left: 4px;
          margin-right: 4px;
          margin-bottom: 0;
          font-weight: 700;
        }

        .u-block-1f24-46 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 4px;
          margin-right: 4px;
          margin-bottom: 0;
        }

        .u-block-1f24-49 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 16px;
          padding-right: 16px;
        }

        .u-block-1f24-50 {
          background-image: url("np://user.desktop.nicepage.com/Site_71051839/images/GiadaMarzi.jpeg?rand=2269");
          height: 199px;
          width: 199px;
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-51 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          margin-top: 32px;
          margin-left: 4px;
          margin-right: 4px;
          margin-bottom: 0;
          font-weight: 700;
        }

        .u-block-1f24-52 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 4px;
          margin-right: 4px;
          margin-bottom: 0;
        }

        .u-block-1f24-59 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 16px;
          padding-right: 16px;
        }

        .u-block-1f24-60 {
          background-image: url("np://user.desktop.nicepage.com/Site_71051839/images/DijetonIsmajli.jpeg?rand=c1b1");
          height: 199px;
          width: 199px;
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-61 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          margin-top: 32px;
          margin-left: 4px;
          margin-right: 4px;
          margin-bottom: 0;
          font-weight: 700;
        }

        .u-block-1f24-62 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 4px;
          margin-right: 4px;
          margin-bottom: 0;
        }

        .u-block-1f24-65 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 16px;
          padding-right: 16px;
        }

        .u-block-1f24-66 {
          background-image: url("np://user.desktop.nicepage.com/Site_71051839/images/7bbd358d8aff93b949ee18e87d41330fb65a4a650d0f2ac7d61372f7355347c999de43b289f395a7c9d16e0f1c73685dca7496bb8570eb9b3df495_1280.png?rand=0d7d");
          height: 199px;
          width: 199px;
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-67 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          margin-top: 32px;
          margin-left: 4px;
          margin-right: 4px;
          margin-bottom: 0;
          font-weight: 700;
        }

        .u-block-1f24-68 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 4px;
          margin-right: 4px;
          margin-bottom: 0;
        }
      }
    </style>
    <style data-mode="MD" data-visited="true">
      @media (max-width: 991px) and (min-width: 768px) {
        .u-block-1f24-1 {
          background-image: none;
        }

        .u-block-1f24-57 {
          min-height: 1806px;
        }

        .u-block-1f24-16 {
          font-weight: 700;
          font-size: 3rem;
          margin-top: 42px;
          margin-left: auto;
          margin-right: auto;
          margin-bottom: 0;
          width: auto;
        }

        .u-block-1f24-28 {
          margin-top: 41px;
          margin-bottom: 42px;
        }

        .u-block-1f24-29 {
          grid-gap: 88px 88px;
          grid-auto-columns: calc(50% - 44.000025px);
          grid-template-columns: repeat(2, calc(50% - 44.000025px));
          min-height: 1600px;
        }

        .u-block-1f24-31 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 16px;
          padding-right: 16px;
        }

        .u-block-1f24-32 {
          background-image: url("np://user.desktop.nicepage.com/Site_71051839/images/thumbnails/ChiaraAntonietti.jpg?rand=62ce");
          height: 199px;
          width: 199px;
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-33 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          margin-top: 32px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
          font-weight: 700;
        }

        .u-block-1f24-34 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-37 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 16px;
          padding-right: 16px;
        }

        .u-block-1f24-38 {
          background-image: url("np://user.desktop.nicepage.com/Site_71051839/images/thumbnails/GuyaNeuroni.jpg?rand=f03a");
          height: 199px;
          width: 199px;
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-39 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          margin-top: 32px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
          font-weight: 700;
        }

        .u-block-1f24-40 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-43 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 16px;
          padding-right: 16px;
        }

        .u-block-1f24-44 {
          background-image: url("np://user.desktop.nicepage.com/Site_71051839/images/thumbnails/SheilaCappelletti.jpg?rand=4afa");
          height: 199px;
          width: 199px;
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-45 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          margin-top: 32px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
          font-weight: 700;
        }

        .u-block-1f24-46 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-49 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 16px;
          padding-right: 16px;
        }

        .u-block-1f24-50 {
          background-image: url("np://user.desktop.nicepage.com/Site_71051839/images/GiadaMarzi.jpeg?rand=2269");
          height: 199px;
          width: 199px;
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-51 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          margin-top: 32px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
          font-weight: 700;
        }

        .u-block-1f24-52 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-59 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 16px;
          padding-right: 16px;
        }

        .u-block-1f24-60 {
          background-image: url("np://user.desktop.nicepage.com/Site_71051839/images/DijetonIsmajli.jpeg?rand=c1b1");
          height: 199px;
          width: 199px;
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-61 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          margin-top: 32px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
          font-weight: 700;
        }

        .u-block-1f24-62 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-65 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 16px;
          padding-right: 16px;
        }

        .u-block-1f24-66 {
          background-image: url("np://user.desktop.nicepage.com/Site_71051839/images/7bbd358d8aff93b949ee18e87d41330fb65a4a650d0f2ac7d61372f7355347c999de43b289f395a7c9d16e0f1c73685dca7496bb8570eb9b3df495_1280.png?rand=0d7d");
          height: 199px;
          width: 199px;
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-67 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          margin-top: 32px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
          font-weight: 700;
        }

        .u-block-1f24-68 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }
      }
    </style>
    <style data-mode="SM">
      @media (max-width: 767px) and (min-width: 576px) {
        .u-block-1f24-1 {
          background-image: none;
        }

        .u-block-1f24-57 {
          min-height: 1806px;
        }

        .u-block-1f24-16 {
          margin-top: 42px;
          margin-left: auto;
          margin-right: auto;
          margin-bottom: 0;
          font-weight: 700;
          font-size: 3rem;
          width: auto;
        }

        .u-block-1f24-28 {
          margin-top: 41px;
          margin-bottom: 42px;
        }

        .u-block-1f24-29 {
          grid-gap: 88px 88px;
          grid-auto-columns: calc(100% - 0px);
          grid-template-columns: 100%;
          min-height: 1600px;
        }

        .u-block-1f24-31 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 10px;
          padding-right: 10px;
        }

        .u-block-1f24-32 {
          background-image: url("np://user.desktop.nicepage.com/Site_71051839/images/thumbnails/ChiaraAntonietti.jpg?rand=62ce");
          height: 199px;
          width: 199px;
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-33 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          margin-top: 32px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
          font-weight: 700;
        }

        .u-block-1f24-34 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-37 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 10px;
          padding-right: 10px;
        }

        .u-block-1f24-38 {
          background-image: url("np://user.desktop.nicepage.com/Site_71051839/images/thumbnails/GuyaNeuroni.jpg?rand=f03a");
          height: 199px;
          width: 199px;
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-39 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          margin-top: 32px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
          font-weight: 700;
        }

        .u-block-1f24-40 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-43 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 10px;
          padding-right: 10px;
        }

        .u-block-1f24-44 {
          background-image: url("np://user.desktop.nicepage.com/Site_71051839/images/thumbnails/SheilaCappelletti.jpg?rand=4afa");
          height: 199px;
          width: 199px;
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-45 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          margin-top: 32px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
          font-weight: 700;
        }

        .u-block-1f24-46 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-49 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 10px;
          padding-right: 10px;
        }

        .u-block-1f24-50 {
          background-image: url("np://user.desktop.nicepage.com/Site_71051839/images/GiadaMarzi.jpeg?rand=2269");
          height: 199px;
          width: 199px;
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-51 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          margin-top: 32px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
          font-weight: 700;
        }

        .u-block-1f24-52 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-59 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 10px;
          padding-right: 10px;
        }

        .u-block-1f24-60 {
          background-image: url("np://user.desktop.nicepage.com/Site_71051839/images/DijetonIsmajli.jpeg?rand=c1b1");
          height: 199px;
          width: 199px;
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-61 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          margin-top: 32px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
          font-weight: 700;
        }

        .u-block-1f24-62 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-65 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 10px;
          padding-right: 10px;
        }

        .u-block-1f24-66 {
          background-image: url("np://user.desktop.nicepage.com/Site_71051839/images/7bbd358d8aff93b949ee18e87d41330fb65a4a650d0f2ac7d61372f7355347c999de43b289f395a7c9d16e0f1c73685dca7496bb8570eb9b3df495_1280.png?rand=0d7d");
          height: 199px;
          width: 199px;
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-67 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          margin-top: 32px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
          font-weight: 700;
        }

        .u-block-1f24-68 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }
      }
    </style>
    <style data-mode="XS">
      @media (max-width: 575px) {
        .u-block-1f24-1 {
          background-image: none;
        }

        .u-block-1f24-57 {
          min-height: 1806px;
        }

        .u-block-1f24-16 {
          margin-top: 42px;
          margin-left: auto;
          margin-right: auto;
          margin-bottom: 0;
          font-weight: 700;
          font-size: 1.5rem;
          width: auto;
        }

        .u-block-1f24-28 {
          margin-top: 41px;
          margin-bottom: 42px;
        }

        .u-block-1f24-29 {
          grid-gap: 88px 88px;
          grid-auto-columns: 100%;
          grid-template-columns: 100%;
          min-height: 1600px;
        }

        .u-block-1f24-31 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 10px;
          padding-right: 10px;
        }

        .u-block-1f24-32 {
          background-image: url("np://user.desktop.nicepage.com/Site_71051839/images/thumbnails/ChiaraAntonietti.jpg?rand=62ce");
          height: 199px;
          width: 199px;
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-33 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          margin-top: 32px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
          font-weight: 700;
        }

        .u-block-1f24-34 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-37 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 10px;
          padding-right: 10px;
        }

        .u-block-1f24-38 {
          background-image: url("np://user.desktop.nicepage.com/Site_71051839/images/thumbnails/GuyaNeuroni.jpg?rand=f03a");
          height: 199px;
          width: 199px;
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-39 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          margin-top: 32px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
          font-weight: 700;
        }

        .u-block-1f24-40 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-43 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 10px;
          padding-right: 10px;
        }

        .u-block-1f24-44 {
          background-image: url("np://user.desktop.nicepage.com/Site_71051839/images/thumbnails/SheilaCappelletti.jpg?rand=4afa");
          height: 199px;
          width: 199px;
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-45 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          margin-top: 32px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
          font-weight: 700;
        }

        .u-block-1f24-46 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-49 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 10px;
          padding-right: 10px;
        }

        .u-block-1f24-50 {
          background-image: url("np://user.desktop.nicepage.com/Site_71051839/images/GiadaMarzi.jpeg?rand=2269");
          height: 199px;
          width: 199px;
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-51 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          margin-top: 32px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
          font-weight: 700;
        }

        .u-block-1f24-52 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-59 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 10px;
          padding-right: 10px;
        }

        .u-block-1f24-60 {
          background-image: url("np://user.desktop.nicepage.com/Site_71051839/images/DijetonIsmajli.jpeg?rand=c1b1");
          height: 199px;
          width: 199px;
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-61 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          margin-top: 32px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
          font-weight: 700;
        }

        .u-block-1f24-62 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }

        .u-block-1f24-65 {
          padding-top: 30px;
          padding-bottom: 30px;
          padding-left: 10px;
          padding-right: 10px;
        }

        .u-block-1f24-66 {
          background-image: url("np://user.desktop.nicepage.com/Site_71051839/images/7bbd358d8aff93b949ee18e87d41330fb65a4a650d0f2ac7d61372f7355347c999de43b289f395a7c9d16e0f1c73685dca7496bb8570eb9b3df495_1280.png?rand=0d7d");
          height: 199px;
          width: 199px;
          background-position: 50% 50%;
          margin-top: 0;
          margin-bottom: 0;
          margin-left: auto;
          margin-right: auto;
        }

        .u-block-1f24-67 {
          font-size: 1.27778rem;
          font-style: normal;
          text-transform: uppercase;
          letter-spacing: 2px;
          margin-top: 32px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
          font-weight: 700;
        }

        .u-block-1f24-68 {
          font-size: 1.125rem;
          font-style: italic;
          margin-top: 20px;
          margin-left: 0;
          margin-right: 0;
          margin-bottom: 0;
        }
      }
    </style>
  </section>



  <footer class="u-clearfix u-footer u-white" id="sec-530e">
    <div class="u-clearfix u-sheet u-sheet-1">
      <div class="u-align-left u-social-icons u-spacing-20 u-social-icons-1">
        <a class="u-social-url" title="facebook" target="_blank" href=""><span class="u-icon u-social-facebook u-social-icon u-icon-1"><svg class="u-svg-link" preserveAspectRatio="xMidYMin slice" viewBox="0 0 112 112">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-f425"></use>
            </svg><svg class="u-svg-content" viewBox="0 0 112 112" x="0" y="0" id="svg-f425">
              <circle fill="currentColor" cx="56.1" cy="56.1" r="55"></circle>
              <path fill="#FFFFFF" d="M73.5,31.6h-9.1c-1.4,0-3.6,0.8-3.6,3.9v8.5h12.6L72,58.3H60.8v40.8H43.9V58.3h-8V43.9h8v-9.2
            c0-6.7,3.1-17,17-17h12.5v13.9H73.5z"></path>
            </svg></span>
        </a>
        <a class="u-social-url" title="instagram" target="_blank" href="https://www.instagram.com/formidabile.ch/"><span class="u-icon u-social-icon u-social-instagram u-icon-2"><svg class="u-svg-link" preserveAspectRatio="xMidYMin slice" viewBox="0 0 112 112">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-eba1"></use>
            </svg><svg class="u-svg-content" viewBox="0 0 112 112" x="0" y="0" id="svg-eba1">
              <circle fill="currentColor" cx="56.1" cy="56.1" r="55"></circle>
              <path fill="#FFFFFF" d="M55.9,38.2c-9.9,0-17.9,8-17.9,17.9C38,66,46,74,55.9,74c9.9,0,17.9-8,17.9-17.9C73.8,46.2,65.8,38.2,55.9,38.2
            z M55.9,66.4c-5.7,0-10.3-4.6-10.3-10.3c-0.1-5.7,4.6-10.3,10.3-10.3c5.7,0,10.3,4.6,10.3,10.3C66.2,61.8,61.6,66.4,55.9,66.4z"></path>
              <path fill="#FFFFFF" d="M74.3,33.5c-2.3,0-4.2,1.9-4.2,4.2s1.9,4.2,4.2,4.2s4.2-1.9,4.2-4.2S76.6,33.5,74.3,33.5z"></path>
              <path fill="#FFFFFF" d="M73.1,21.3H38.6c-9.7,0-17.5,7.9-17.5,17.5v34.5c0,9.7,7.9,17.6,17.5,17.6h34.5c9.7,0,17.5-7.9,17.5-17.5V38.8
            C90.6,29.1,82.7,21.3,73.1,21.3z M83,73.3c0,5.5-4.5,9.9-9.9,9.9H38.6c-5.5,0-9.9-4.5-9.9-9.9V38.8c0-5.5,4.5-9.9,9.9-9.9h34.5
            c5.5,0,9.9,4.5,9.9,9.9V73.3z"></path>
            </svg></span>
        </a>
        <a class="u-social-url" title="linkedin" target="_blank" href=""><span class="u-icon u-social-icon u-social-linkedin u-icon-3"><svg class="u-svg-link" preserveAspectRatio="xMidYMin slice" viewBox="0 0 112 112">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-f491"></use>
            </svg><svg class="u-svg-content" viewBox="0 0 112 112" x="0" y="0" id="svg-f491">
              <circle fill="currentColor" cx="56.1" cy="56.1" r="55"></circle>
              <path fill="#FFFFFF" d="M41.3,83.7H27.9V43.4h13.4V83.7z M34.6,37.9L34.6,37.9c-4.6,0-7.5-3.1-7.5-7c0-4,3-7,7.6-7s7.4,3,7.5,7
            C42.2,34.8,39.2,37.9,34.6,37.9z M89.6,83.7H76.2V62.2c0-5.4-1.9-9.1-6.8-9.1c-3.7,0-5.9,2.5-6.9,4.9c-0.4,0.9-0.4,2.1-0.4,3.3v22.5
            H48.7c0,0,0.2-36.5,0-40.3h13.4v5.7c1.8-2.7,5-6.7,12.1-6.7c8.8,0,15.4,5.8,15.4,18.1V83.7z"></path>
            </svg></span>
        </a>
      </div>
      <div class="u-border-1 u-border-black u-expanded-width u-line u-line-horizontal u-opacity u-opacity-50 u-line-1"></div>
      <p class="u-align-center u-text u-text-1">Copyright&copy;; FormidAbile 2022 | Web Design by Giada Marzi
      </p>
    </div>
  </footer>

</body>

</html>