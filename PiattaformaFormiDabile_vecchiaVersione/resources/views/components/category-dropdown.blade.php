<x-dropdown>

<x-slot name="trigger">
  <button class="py-2 pl-3 pr-9 text-sm font-semibold w-full lg:w-32 text-left flex lg:inline-flex">
    {{ isset($currentCategory) ? ucwords($currentCategory->name) : 'Categories' }}

    <x-icon name="down-arrow" class="absolute pointer-events-none" style="right: 12px;" />

  </button>
</x-slot>


<!-- IL PROBLEMA SUSSISTE QUI, LE DUE OPTION NON RIESCONO A VISUALIZZARE LA SLUG, UNA VOLTA
CHE SI CLICCA SULL'OPZIONE, NON SUCCEDE NULLA. IN TEORIA SI DOVREBBE VEDERE LA CATEGORIA -->
<option>
  <x-dropdown-item href="/risorse-documentazione" :active="request()->routeIs('documentazione')">
    All
  </x-dropdown-item>
</option>


<!-- Seleziona la categoria che l'utente sceglie e mostra solo i post con quella categoria -->
@foreach ($categories as $category)
<option>

        <x-dropdown-item href="/?category={{ $category->slug }}&{{ http_build_query(request()->except('category', 'page')) }}"
         :active='request()->is("categories/.{$category->slug}")'>
            {{ ucwords($category->name) }}
        </x-dropdown-item>
     
</option>
@endforeach


</x-dropdown>