@props(['name'])

@if ($name === 'down-arrow')

<svg xmlns="http://www.w3.org/2000/svg" {{ $attributes(['class' => 'transform -rotate-90']) }} width="14" height="12" version="1" class="u-caret">
    <g fill="none" fill-rule="evenodd">
        <path fill="currentColor" d="M4 8L0 4h8z"></path>
    </g>
</svg>
@endif