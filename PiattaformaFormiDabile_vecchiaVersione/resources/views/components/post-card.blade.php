@props(['post'])


<section class="u-clearfix u-grey-5 u-section-2" id="sec-c98e">
    <div class="py-6 px-5">
        <div>
            <div class="u-repeater u-repeater-1" style="display: inline;">
                <div class="mt-8 flex flex-col justify-between" style="margin:20px">
                    <header>
                        <div class="space-x-2">
                            <!-- <a href="/categories/{{ $post->category->slug }}" class="px-3 py-1 border border-blue-300 rounded-full text-blue-300 text-xs uppercase font-semibold" style="font-size: 10px">{{ $post->category->name }}</a> -->
                            <x-category-button :category="$post->category" />
                        </div>

                        <div class="mt-4">
                            <h1 class="text-3xl clamp one-line">
                                <a href="/posts/{{ $post->slug }}" style="color: #E3256E">
                                    {{ $post->title }}
                                </a>
                            </h1>

                            <span class="mt-2 block text-gray-400 text-xs">
                                Published <time>{{ $post->created_at->diffForHumans() }}</time>
                            </span>
                        </div>
                    </header>

                    <div class="text-sm mt-4 space-y-4" style="font-size: 16px">

                        {!! $post->excerpt !!}

                    </div>
                    <footer class="flex justify-between items-center mt-8">
                        <div class="flex items-center text-sm">
                            <div class="ml-3">

                                <h5 class="font-bold">
                                    <a href="/?authors={{ $post->author->username }}"  style="margin: auto; color: #E3256E">
                                        {{ $post->author->name }}</a>
                                </h5>

                            </div>
                        </div>

                        <div>
                            <a href="/posts/{{ $post->slug }}" class="transition-colors duration-300 text-xs font-semibold border-red-500 bg-gray-200 hover:bg-gray-300 rounded-full py-2 px-8 " style="color: #E3256E">Read More</a>
                        </div>
                    </footer>
                </div>
            </div>
        </div>
    </div>
</section>
<div style="height: 50px; background: #ffffff;"></div>