<!DOCTYPE html>
<html style="font-size: 16px;" lang="it-CH">

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta charset="utf-8">
  <meta name="keywords" content="Progetto FormidAbile">
  <meta name="description" content="La piattaforma FormidAbile rende accessibili informazioni affidabili riguardanti disabilità e difficoltà fisiche, cognitive e psichiche dei giovani, e mette a disposizione materiali utili per la progettazione di attività formative ed educative inclusive.">
  <title>FormidAbile - Associazioni</title>
  <meta name="generator" content="Nicepage 4.18.5, nicepage.com">
  <meta property="og:title" content="Piattaforma FormidAbile">
  <meta property="og:description" content="La piattaforma FormidAbile rende accessibili informazioni affidabili riguardanti disabilità e difficoltà fisiche, cognitive e psichiche dei giovani, e mette a disposizione materiali utili per la progettazione di attività formative ed educative inclusive.">
  <meta property="og:image" content="images/Immagine21.png">
  <meta property="og:url" content="formidabile.ch">
  <link rel="canonical" href="www.formidabile.ch">
  <link rel="stylesheet" href="{{url('/css/nicepage.css')}}" media="screen">
  <link rel="stylesheet" href="{{url('/css/RisorseAssociazioni.css')}}" media="screen">
  <script class="u-script" type="text/javascript" src="{{url('/js/jquery.js')}}" "="" defer=""></script>
    <script class=" u-script" type="text/javascript" src="{{url('/js/nicepage.js')}}" "="" defer=""></script>
    <meta name=" generator" content="Nicepage 4.16.0, nicepage.com">
    <link id = "u-theme-google-font"
    rel = "stylesheet"
    href = "fonts.css" >
      <link id = "u-page-google-font"
    rel = "stylesheet"
    href = "RisorseAssociazioni-fonts.css" >


      <script type = "application/ld+json" > {
        "@context": "http://schema.org",
        "@type": "Organization",
        "name": "Piattaforma FormiDabile",
        "logo": "images/Immagine21.png",
        "sameAs": []
      }
  </script>
  <meta name="theme-color" content="#9b74ec">
  <meta property="og:title" content="RisorseAssociazioni">
  <meta property="og:description" content="">
  <meta property="og:type" content="website">
</head>

<body class="u-body u-xl-mode" data-lang="it">
  <header class="u-clearfix u-header u-white" id="sec-f943" data-animation-name="" data-animation-duration="0" data-animation-delay="0" data-animation-direction="">
    <div class="u-clearfix u-sheet u-valign-middle-lg u-valign-middle-xl u-sheet-1">
      <a href="Home.html" data-page-id="236022295" class="u-image u-logo u-image-1" data-image-width="1920" data-image-height="1725" title="Home">
        <img src="images/Immagine21.png" class="u-logo-image u-logo-image-1">
      </a>
      <a class="u-login u-text-body-color u-text-hover-custom-color-28 u-login-1" href="#" title="Login" target="_blank">Login</a>
      <nav class="u-menu u-menu-one-level u-offcanvas u-offcanvas-shift u-menu-1">
        <div class="menu-collapse" style="font-size: 1.125rem; letter-spacing: 0px; text-transform: uppercase; font-weight: 500;">
          <a class="u-button-style u-custom-active-border-color u-custom-active-color u-custom-border u-custom-border-color u-custom-borders u-custom-hover-border-color u-custom-hover-color u-custom-left-right-menu-spacing u-custom-padding-bottom u-custom-text-active-color u-custom-text-color u-custom-text-decoration u-custom-text-hover-color u-custom-top-bottom-menu-spacing u-nav-link" href="#" style="font-size: calc(1em + 16px); padding: 8px 18px; background-image: none;">
            <svg class="u-svg-link" viewBox="0 0 24 24">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-7d29"></use>
            </svg>
            <svg class="u-svg-content" version="1.1" id="svg-7d29" viewBox="0 0 16 16" x="0px" y="0px" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
              <g>
                <rect y="1" width="16" height="2"></rect>
                <rect y="7" width="16" height="2"></rect>
                <rect y="13" width="16" height="2"></rect>
              </g>
            </svg>
          </a>
        </div>
        <div class="u-custom-menu u-nav-container">
          <ul class="u-nav u-spacing-0 u-unstyled u-nav-1">
            <li class="u-nav-item"><a class="u-border-active-palette-1-base u-border-hover-palette-1-light-1 u-border-no-left u-border-no-right u-border-no-top u-button-style u-nav-link u-text-active-custom-color-27 u-text-hover-custom-color-28" href="/home" style="padding: 2px 20px;">Home</a>
            </li>
            <li class="u-nav-item"><a class="u-border-active-palette-1-base u-border-hover-palette-1-light-1 u-border-no-left u-border-no-right u-border-no-top u-button-style u-nav-link u-text-active-custom-color-27 u-text-hover-custom-color-28" href="/chi-siamo" style="padding: 2px 20px;">Chi Siamo</a>
            </li>
            <li class="u-nav-item"><a class="u-border-active-palette-1-base u-border-hover-palette-1-light-1 u-border-no-left u-border-no-right u-border-no-top u-button-style u-nav-link u-text-active-custom-color-27 u-text-hover-custom-color-28" href="/risorse" style="padding: 2px 20px;">Risorse</a>
            </li>
            <li class="u-nav-item"><a class="u-border-active-palette-1-base u-border-hover-palette-1-light-1 u-border-no-left u-border-no-right u-border-no-top u-button-style u-nav-link u-text-active-custom-color-27 u-text-hover-custom-color-28" href="/collabora-con-noi" style="padding: 2px 20px;">Collabora con noi</a>
            </li>
            <li class="u-nav-item"><a class="u-border-active-palette-1-base u-border-hover-palette-1-light-1 u-border-no-left u-border-no-right u-border-no-top u-button-style u-nav-link u-text-active-custom-color-27 u-text-hover-custom-color-28" href="/contatti" style="padding: 2px 20px;">Contatti</a>
            </li>
            <li class="u-nav-item"><a class="u-border-active-palette-1-base u-border-hover-palette-1-light-1 u-border-no-left u-border-no-right u-border-no-top u-button-style u-nav-link u-text-active-custom-color-27 u-text-hover-custom-color-28" style="padding: 2px 20px;">Prezzi</a>
            </li>
          </ul>
        </div>
        <div class="u-custom-menu u-nav-container-collapse">
          <div class="u-black u-container-style u-inner-container-layout u-opacity u-opacity-95 u-sidenav">
            <div class="u-inner-container-layout u-sidenav-overflow">
              <div class="u-menu-close"></div>
              <ul class="u-align-center u-nav u-popupmenu-items u-unstyled u-nav-2">
                <li class="u-nav-item"><a class="u-button-style u-nav-link" href="/home">Home</a>
                </li>
                <li class="u-nav-item"><a class="u-button-style u-nav-link" href="/chi-siamo">Chi Siamo</a>
                </li>
                <li class="u-nav-item"><a class="u-button-style u-nav-link" href="/risorse">Risorse</a>
                </li>
                <li class="u-nav-item"><a class="u-button-style u-nav-link" href="/collabora-con-noi">Collabora con noi</a>
                </li>
                <li class="u-nav-item"><a class="u-button-style u-nav-link" href="/contatti">Contatti</a>
                </li>
                <li class="u-nav-item"><a class="u-button-style u-nav-link">Prezzi</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="u-black u-menu-overlay u-opacity u-opacity-70"></div>
        </div>
      </nav>
    </div>
  </header>
  <section class="u-align-center u-clearfix u-gradient u-section-1" src="" id="carousel_e115">
    <div class="u-expanded-width u-grey-5 u-shape u-shape-rectangle u-shape-1"></div>
    <h2 class="u-custom-font u-font-montserrat u-text u-text-body-color u-text-default-lg u-text-default-xl u-text-1">
      <span class="u-text-custom-color-27">Con</span>
      <span class="u-text-custom-color-43">tat</span>
      <span class="u-text-custom-color-42">ti</span>
      <span class="u-text-custom-color-42"> As</span>
      <span class="u-text-custom-color-20">socia</span>
      <span class="u-text-custom-color-44">zioni</span>
    </h2>
    <p class="u-text u-text-body-color u-text-2"> Lista delle associazioni che collaborano con noi<span style="font-style: normal;"></span>
    </p>
    <div class="u-list u-list-1">
      <div class="u-repeater u-repeater-1">
        <div class="u-align-center u-border-1 u-border-grey-10 u-container-style u-list-item u-repeater-item u-shape-rectangle u-white u-list-item-1">
          <div class="u-container-layout u-similar-container u-container-layout-1">
            <img alt="" class="u-image u-image-contain u-image-round u-radius-30 u-image-1" src="images/logoUnitas.svg" data-image-width="1247" data-image-height="419">
            <h5 class="u-align-center u-text u-text-custom-color-4 u-text-3"> Associazione ciechi ed ipovedenti della Svizzzera Italiana (UNITAS)</h5>
            <p class="u-align-center u-text u-text-body-color u-text-4">Via San Gottardo 49<br>6598 Tenero<br>
            </p>
            <a href="https://nicepage.com/wysiwyg-html-editor" class="u-active-none u-bottom-left-radius-0 u-bottom-right-radius-0 u-btn u-btn-rectangle u-button-style u-hover-none u-none u-radius-0 u-top-left-radius-0 u-top-right-radius-0 u-btn-1"><span class="u-icon"><svg class="u-svg-content" viewBox="0 0 405.333 405.333" x="0px" y="0px" style="width: 1em; height: 1em;">
                  <path d="M373.333,266.88c-25.003,0-49.493-3.904-72.704-11.563c-11.328-3.904-24.192-0.896-31.637,6.699l-46.016,34.752    c-52.8-28.181-86.592-61.952-114.389-114.368l33.813-44.928c8.512-8.512,11.563-20.971,7.915-32.64    C142.592,81.472,138.667,56.96,138.667,32c0-17.643-14.357-32-32-32H32C14.357,0,0,14.357,0,32    c0,205.845,167.488,373.333,373.333,373.333c17.643,0,32-14.357,32-32V298.88C405.333,281.237,390.976,266.88,373.333,266.88z"></path>
                </svg><img></span>&nbsp;(+41) 91 735 69 00
            </a>
            <a href="mailto:info@unitas.ch" class="u-border-1 u-border-active-palette-2-base u-border-hover-palette-1-base u-btn u-button-style u-none u-text-custom-color-4 u-btn-2">info@unitas.ch</a>
            <a href="www.unitas.ch" class="u-border-1 u-border-active-palette-2-base u-border-hover-palette-1-base u-btn u-button-style u-none u-text-custom-color-4 u-btn-3">unitas.ch</a>
          </div>
        </div>
        <div class="u-align-center u-border-1 u-border-grey-10 u-container-style u-list-item u-repeater-item u-shape-rectangle u-video-cover u-white">
          <div class="u-container-layout u-similar-container u-container-layout-2">
            <img alt="" class="u-image u-image-contain u-image-round u-radius-30 u-image-2" src="images/asi.png" data-image-width="408" data-image-height="102">
            <h5 class="u-align-center u-text u-text-custom-color-4 u-text-5"> Associazione Autismo Svizzera Italiana </h5>
            <p class="u-align-center u-text u-text-body-color u-text-6"> Via Medi 8<br>6932 Breganzona<br>
            </p>
            <a href="https://nicepage.com/wysiwyg-html-editor" class="u-active-none u-bottom-left-radius-0 u-bottom-right-radius-0 u-btn u-btn-rectangle u-button-style u-hover-none u-none u-radius-0 u-top-left-radius-0 u-top-right-radius-0 u-btn-4"><span class="u-icon"><svg class="u-svg-content" viewBox="0 0 405.333 405.333" x="0px" y="0px">
                  <path d="M373.333,266.88c-25.003,0-49.493-3.904-72.704-11.563c-11.328-3.904-24.192-0.896-31.637,6.699l-46.016,34.752    c-52.8-28.181-86.592-61.952-114.389-114.368l33.813-44.928c8.512-8.512,11.563-20.971,7.915-32.64    C142.592,81.472,138.667,56.96,138.667,32c0-17.643-14.357-32-32-32H32C14.357,0,0,14.357,0,32    c0,205.845,167.488,373.333,373.333,373.333c17.643,0,32-14.357,32-32V298.88C405.333,281.237,390.976,266.88,373.333,266.88z"></path>
                </svg><img></span> &nbsp;(+41) 91 857 99 33
            </a>
            <a href="mailto:info@unitas.ch" class="u-border-1 u-border-active-palette-2-base u-border-hover-palette-1-base u-btn u-button-style u-none u-text-custom-color-4 u-btn-5"> info@autismo.ch</a>
            <a href="www.unitas.ch" class="u-border-1 u-border-active-palette-2-base u-border-hover-palette-1-base u-btn u-button-style u-none u-text-custom-color-4 u-btn-6"> autismo.ch</a>
          </div>
        </div>
        <div class="u-border-1 u-border-grey-10 u-container-style u-list-item u-repeater-item u-shape-rectangle u-video-cover u-white">
          <div class="u-container-layout u-similar-container u-container-layout-3">
            <img alt="" class="u-image u-image-contain u-image-round u-radius-30 u-image-3" src="images/sgbfss1.png" data-image-width="276" data-image-height="75">
            <h5 class="u-align-center u-text u-text-custom-color-4 u-text-7"> Federazione svizzera dei sordi</h5>
            <p class="u-align-center u-text u-text-body-color u-text-8"> Via Besso 5<br>6900 Lugano<br>
            </p>
            <a href="https://nicepage.com/wysiwyg-html-editor" class="u-active-none u-bottom-left-radius-0 u-bottom-right-radius-0 u-btn u-btn-rectangle u-button-style u-hover-none u-none u-radius-0 u-top-left-radius-0 u-top-right-radius-0 u-btn-7"><span class="u-icon"><svg class="u-svg-content" viewBox="0 0 405.333 405.333" x="0px" y="0px">
                  <path d="M373.333,266.88c-25.003,0-49.493-3.904-72.704-11.563c-11.328-3.904-24.192-0.896-31.637,6.699l-46.016,34.752    c-52.8-28.181-86.592-61.952-114.389-114.368l33.813-44.928c8.512-8.512,11.563-20.971,7.915-32.64    C142.592,81.472,138.667,56.96,138.667,32c0-17.643-14.357-32-32-32H32C14.357,0,0,14.357,0,32    c0,205.845,167.488,373.333,373.333,373.333c17.643,0,32-14.357,32-32V298.88C405.333,281.237,390.976,266.88,373.333,266.88z"></path>
                </svg><img></span> &nbsp;(+41) 91 950 05 48
            </a>
            <a href="mailto:info@unitas.ch" class="u-border-1 u-border-active-palette-2-base u-border-hover-palette-1-base u-btn u-button-style u-none u-text-custom-color-4 u-btn-8"> info-i@sgb-fss.ch</a>
            <a href="www.unitas.ch" class="u-border-1 u-border-active-palette-2-base u-border-hover-palette-1-base u-btn u-button-style u-none u-text-custom-color-4 u-btn-9"> sgb-fss.ch</a>
          </div>
        </div>
        <div class="u-border-1 u-border-grey-10 u-container-style u-list-item u-repeater-item u-shape-rectangle u-video-cover u-white">
          <div class="u-container-layout u-similar-container u-container-layout-4">
            <img alt="" class="u-image u-image-contain u-image-round u-radius-30 u-image-4" data-image-width="150" data-image-height="94" src="images/fondazione-diamante.gif">
            <h5 class="u-align-center u-text u-text-custom-color-4 u-text-9"> Fondazione Diamante</h5>
            <p class="u-align-center u-text u-text-body-color u-text-10"> Via Molino 1<br>6928 Manno<br>
            </p>
            <a href="https://nicepage.com/wysiwyg-html-editor" class="u-active-none u-bottom-left-radius-0 u-bottom-right-radius-0 u-btn u-btn-rectangle u-button-style u-hover-none u-none u-radius-0 u-top-left-radius-0 u-top-right-radius-0 u-btn-10"><span class="u-icon"><svg class="u-svg-content" viewBox="0 0 405.333 405.333" x="0px" y="0px">
                  <path d="M373.333,266.88c-25.003,0-49.493-3.904-72.704-11.563c-11.328-3.904-24.192-0.896-31.637,6.699l-46.016,34.752    c-52.8-28.181-86.592-61.952-114.389-114.368l33.813-44.928c8.512-8.512,11.563-20.971,7.915-32.64    C142.592,81.472,138.667,56.96,138.667,32c0-17.643-14.357-32-32-32H32C14.357,0,0,14.357,0,32    c0,205.845,167.488,373.333,373.333,373.333c17.643,0,32-14.357,32-32V298.88C405.333,281.237,390.976,266.88,373.333,266.88z"></path>
                </svg><img></span> &nbsp;(+41) 91 610 00 20
            </a>
            <a href="mailto:info@unitas.ch" class="u-border-1 u-border-active-palette-2-base u-border-hover-palette-1-base u-btn u-button-style u-none u-text-custom-color-4 u-btn-11"> info@f-diamante.ch</a>
            <a href="www.unitas.ch" class="u-border-1 u-border-active-palette-2-base u-border-hover-palette-1-base u-btn u-button-style u-none u-text-custom-color-4 u-btn-12"> f-diamante.ch</a>
          </div>
        </div>
        <div class="u-border-1 u-border-grey-10 u-container-style u-list-item u-repeater-item u-shape-rectangle u-video-cover u-white">
          <div class="u-container-layout u-similar-container u-container-layout-5">
            <img alt="" class="u-image u-image-contain u-image-round u-radius-30 u-image-5" data-image-width="332" data-image-height="317" src="images/simbolo.png">
            <h5 class="u-align-center u-text u-text-custom-color-4 u-text-11"> Associazione DSA ADHD Ticino (ADAT)</h5>
            <p class="u-align-center u-text u-text-body-color u-text-12"> Viale Portone 12<br>6501 Bellinzona<br>
            </p>
            <a href="https://nicepage.com/wysiwyg-html-editor" class="u-active-none u-bottom-left-radius-0 u-bottom-right-radius-0 u-btn u-btn-rectangle u-button-style u-hover-none u-none u-radius-0 u-top-left-radius-0 u-top-right-radius-0 u-btn-13"><span class="u-icon"><svg class="u-svg-content" viewBox="0 0 405.333 405.333" x="0px" y="0px" style="width: 1em; height: 1em;">
                  <path d="M373.333,266.88c-25.003,0-49.493-3.904-72.704-11.563c-11.328-3.904-24.192-0.896-31.637,6.699l-46.016,34.752    c-52.8-28.181-86.592-61.952-114.389-114.368l33.813-44.928c8.512-8.512,11.563-20.971,7.915-32.64    C142.592,81.472,138.667,56.96,138.667,32c0-17.643-14.357-32-32-32H32C14.357,0,0,14.357,0,32    c0,205.845,167.488,373.333,373.333,373.333c17.643,0,32-14.357,32-32V298.88C405.333,281.237,390.976,266.88,373.333,266.88z"></path>
                </svg><img></span>&nbsp;(+41) 91 735 69 00
            </a>
            <a href="mailto:info@unitas.ch" class="u-border-1 u-border-active-palette-2-base u-border-hover-palette-1-base u-btn u-button-style u-none u-text-custom-color-4 u-btn-14"> associazioneadat@gmail.com</a>
            <a href="www.unitas.ch" class="u-border-1 u-border-active-palette-2-base u-border-hover-palette-1-base u-btn u-button-style u-none u-text-custom-color-4 u-btn-15"> associazioneadat.com</a>
          </div>
        </div>
        <div class="u-border-1 u-border-grey-10 u-container-style u-list-item u-repeater-item u-shape-rectangle u-video-cover u-white">
          <div class="u-container-layout u-similar-container u-container-layout-6">
            <img alt="" class="u-image u-image-contain u-image-round u-radius-30 u-image-6" data-image-width="177" data-image-height="70" src="images/iat-logo-small-1.png">
            <h5 class="u-align-center u-text u-text-custom-color-4 u-text-13"> Inclusione andicap Ticino</h5>
            <p class="u-align-center u-text u-text-body-color u-text-14"> Via Linoleum 7<br>Casella postale 572&nbsp;<br>6512 Giubiasco<br>
            </p>
            <a href="https://nicepage.com/wysiwyg-html-editor" class="u-active-none u-bottom-left-radius-0 u-bottom-right-radius-0 u-btn u-btn-rectangle u-button-style u-hover-none u-none u-radius-0 u-top-left-radius-0 u-top-right-radius-0 u-btn-16"><span class="u-icon"><svg class="u-svg-content" viewBox="0 0 405.333 405.333" x="0px" y="0px">
                  <path d="M373.333,266.88c-25.003,0-49.493-3.904-72.704-11.563c-11.328-3.904-24.192-0.896-31.637,6.699l-46.016,34.752    c-52.8-28.181-86.592-61.952-114.389-114.368l33.813-44.928c8.512-8.512,11.563-20.971,7.915-32.64    C142.592,81.472,138.667,56.96,138.667,32c0-17.643-14.357-32-32-32H32C14.357,0,0,14.357,0,32    c0,205.845,167.488,373.333,373.333,373.333c17.643,0,32-14.357,32-32V298.88C405.333,281.237,390.976,266.88,373.333,266.88z"></path>
                </svg><img></span> &nbsp;(+41) 91 850 90 90
            </a>
            <a href="mailto:info@unitas.ch" class="u-border-1 u-border-active-palette-2-base u-border-hover-palette-1-base u-btn u-button-style u-none u-text-custom-color-4 u-btn-17"> info@inclusione-andicap-ticino.ch</a>
            <a href="www.unitas.ch" class="u-border-1 u-border-active-palette-2-base u-border-hover-palette-1-base u-btn u-button-style u-none u-text-custom-color-4 u-btn-18"> inclusione-andicap-ticino.ch</a>
          </div>
        </div>
      </div>
    </div>
  </section>


  <footer class="u-clearfix u-footer u-white" id="sec-530e">
    <div class="u-clearfix u-sheet u-sheet-1">
      <div class="u-align-left u-social-icons u-spacing-20 u-social-icons-1">
        <a class="u-social-url" title="facebook" target="_blank" href=""><span class="u-icon u-social-facebook u-social-icon u-icon-1"><svg class="u-svg-link" preserveAspectRatio="xMidYMin slice" viewBox="0 0 112 112">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-f425"></use>
            </svg><svg class="u-svg-content" viewBox="0 0 112 112" x="0" y="0" id="svg-f425">
              <circle fill="currentColor" cx="56.1" cy="56.1" r="55"></circle>
              <path fill="#FFFFFF" d="M73.5,31.6h-9.1c-1.4,0-3.6,0.8-3.6,3.9v8.5h12.6L72,58.3H60.8v40.8H43.9V58.3h-8V43.9h8v-9.2
            c0-6.7,3.1-17,17-17h12.5v13.9H73.5z"></path>
            </svg></span>
        </a>
        <a class="u-social-url" title="instagram" target="_blank" href="https://www.instagram.com/formidabile.ch/"><span class="u-icon u-social-icon u-social-instagram u-icon-2"><svg class="u-svg-link" preserveAspectRatio="xMidYMin slice" viewBox="0 0 112 112">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-eba1"></use>
            </svg><svg class="u-svg-content" viewBox="0 0 112 112" x="0" y="0" id="svg-eba1">
              <circle fill="currentColor" cx="56.1" cy="56.1" r="55"></circle>
              <path fill="#FFFFFF" d="M55.9,38.2c-9.9,0-17.9,8-17.9,17.9C38,66,46,74,55.9,74c9.9,0,17.9-8,17.9-17.9C73.8,46.2,65.8,38.2,55.9,38.2
            z M55.9,66.4c-5.7,0-10.3-4.6-10.3-10.3c-0.1-5.7,4.6-10.3,10.3-10.3c5.7,0,10.3,4.6,10.3,10.3C66.2,61.8,61.6,66.4,55.9,66.4z"></path>
              <path fill="#FFFFFF" d="M74.3,33.5c-2.3,0-4.2,1.9-4.2,4.2s1.9,4.2,4.2,4.2s4.2-1.9,4.2-4.2S76.6,33.5,74.3,33.5z"></path>
              <path fill="#FFFFFF" d="M73.1,21.3H38.6c-9.7,0-17.5,7.9-17.5,17.5v34.5c0,9.7,7.9,17.6,17.5,17.6h34.5c9.7,0,17.5-7.9,17.5-17.5V38.8
            C90.6,29.1,82.7,21.3,73.1,21.3z M83,73.3c0,5.5-4.5,9.9-9.9,9.9H38.6c-5.5,0-9.9-4.5-9.9-9.9V38.8c0-5.5,4.5-9.9,9.9-9.9h34.5
            c5.5,0,9.9,4.5,9.9,9.9V73.3z"></path>
            </svg></span>
        </a>
        <a class="u-social-url" title="linkedin" target="_blank" href=""><span class="u-icon u-social-icon u-social-linkedin u-icon-3"><svg class="u-svg-link" preserveAspectRatio="xMidYMin slice" viewBox="0 0 112 112">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-f491"></use>
            </svg><svg class="u-svg-content" viewBox="0 0 112 112" x="0" y="0" id="svg-f491">
              <circle fill="currentColor" cx="56.1" cy="56.1" r="55"></circle>
              <path fill="#FFFFFF" d="M41.3,83.7H27.9V43.4h13.4V83.7z M34.6,37.9L34.6,37.9c-4.6,0-7.5-3.1-7.5-7c0-4,3-7,7.6-7s7.4,3,7.5,7
            C42.2,34.8,39.2,37.9,34.6,37.9z M89.6,83.7H76.2V62.2c0-5.4-1.9-9.1-6.8-9.1c-3.7,0-5.9,2.5-6.9,4.9c-0.4,0.9-0.4,2.1-0.4,3.3v22.5
            H48.7c0,0,0.2-36.5,0-40.3h13.4v5.7c1.8-2.7,5-6.7,12.1-6.7c8.8,0,15.4,5.8,15.4,18.1V83.7z"></path>
            </svg></span>
        </a>
      </div>
      <div class="u-border-1 u-border-black u-expanded-width u-line u-line-horizontal u-opacity u-opacity-50 u-line-1"></div>
      <p class="u-align-center u-text u-text-1">Copyright&copy;; Formidabile 2022 | Web Design by Giada Marzi
      </p>
    </div>
  </footer>


</body>

</html>