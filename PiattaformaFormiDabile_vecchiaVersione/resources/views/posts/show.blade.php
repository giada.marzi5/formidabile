<x-layout>


    <section class="u-clearfix u-grey-5 u-section-1" id="sec-ed8b">
        <main class="max-w-6xl mx-auto mt-10 lg:mt-20 space-y-6" style="margin-bottom: 65px;">
            <article class="max-w-4xl mx-auto lg:grid lg:grid-cols-12 gap-x-10">

                <div class="col-span-12">
                    <div class="hidden lg:flex justify-between mb-6">
                        <a href="/risorse-documentazione" class="transition-colors duration-300 relative inline-flex items-center text-lg hover:text-red-500" style="color:#E3256E">
                            <svg width="22" height="22" viewBox="0 0 22 22" class="mr-2">
                                <g fill="none" fill-rule="evenodd">
                                    <path stroke="#000" stroke-opacity=".012" stroke-width=".5" d="M21 1v20.16H.84V1z">
                                    </path>
                                    <path class="fill-current" d="M13.854 7.224l-3.847 3.856 3.847 3.856-1.184 1.184-5.04-5.04 5.04-5.04z">
                                    </path>
                                </g>
                            </svg>

                            Torna indietro
                        </a>

                        <div class="space-x-2">
                            <x-category-button :category="$post->category" />

                        </div>
                    </div>

                    <h1 class="font-bold text-3xl lg:text-4xl mb-10">
                        {{ $post->title }}
                    </h1>

                    <div>
                        <p class="block text-gray-400 text-xs">
                            Published <time>{{ $post->created_at->diffForHumans() }}</time></p>
                        <p style="text-align:end; color:#9CA3AF; font-size:12px"> Author: {{ $post->author->name }}   </p>
                        <!-- <a href="/?authors={{ $post->author->username }}">{{ $post->author->name }}</a> -->
                        <div class="flex items-center lg:justify-center text-sm mt-4">
                            <div class="text-left">


                            </div>
                        </div>
                    </div>

                    <div class="space-y-4 lg:text-lg leading-loose">
                        {!! $post->body !!}
                    </div>


                </div>
            </article>
        </main>
    </section>

</x-layout>