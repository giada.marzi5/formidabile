<section class="u-align-center u-clearfix u-grey-5 u-section-1" id="carousel_2933">
  <div class="u-clearfix u-sheet u-sheet-1">
    <div class="u-gradient u-shape u-shape-rectangle u-shape-1"></div>
    <div class="u-container-style u-group u-shape-rectangle u-white u-group-1" style="margin: 35px">

      <div class="u-container-layout u-container-layout-1">

        <h2 class="u-align-center u-custom-font u-font-montserrat u-text u-text-default u-text-1" style="margin: 10px auto 20px">
          <span class="u-text-custom-color-27">Do</span>
          <span class="u-text-custom-color-43">cu</span>
          <span class="u-text-custom-color-42">men</span>
          <span class="u-text-custom-color-20">taz</span>
          <span class="u-text-custom-color-44">ione<span style="font-weight: 700;"></span>
          </span>
        </h2>


        <!-- form per il Search -->
        <form action="#" method="GET" class="u-border-1 u-border-grey-30 u-grey-5 u-search u-search-left u-search-1" style="margin: auto 45px auto">
          @if (request('category'))
          <input type="hidden" name="category" value="{{ request('category') }}">
          @endif
          <input type="text" name="search" placeholder="Search" class="bg-transparent placeholder-black font-semibold text-sm" value="{{ request('search') }}">
        </form>


        <!-- form per le altre caselle di ricerca -->
        <div class="u-align-center-sm u-align-center-xs u-form u-form-1">
          <form action="/" method="get" class="u-clearfix u-form-spacing-50 u-form-vertical u-inner-form" style="padding: 50px;" source="custom" name="form" data-services="c30642826862e5a120a40e4dec6536bf">

            <div class="u-form-group u-form-select u-form-group-1">
              <label for="select-2d8c" class="u-label u-label-1">DISTURBO E DISABILITÀ</label>
              <div class="u-form-select-wrapper">
                <select id="select-2d8c" class="relative lg:inline-flex u-grey-5 u-input u-input-rectangle u-input-1" style="appearance: auto;">

                  <x-category-dropdown/>
                </select>
              </div>
            </div>


            <div class="u-form-group u-form-select u-form-group-2">
              <label for="select-4cc7" class="u-label u-label-2">DIFFICOLTÀ</label>
              <div class="u-form-select-wrapper">
                <select id="select-4cc7" name="select-2" class="u-grey-5 u-input u-input-rectangle u-input-2">
                  <option value="Scegli un opzione...">Scegli un opzione...</option>
                  <option value="Ascolto">Ascolto</option>
                  <option value="Attenzione Calcolo">Attenzione Calcolo</option>
                  <option value="Difficoltà di lettura">Difficoltà di lettura</option>
                  <option value="Difficoltà nel memorizzare">Difficoltà nel memorizzare</option>
                  <option value="Movimento">Movimento</option>
                  <option value="Difficoltà di scrittura">Difficoltà di scrittura</option>
                  <option value="Visione">Visione</option>
                </select>
                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="12" version="1" class="u-caret">
                  <path fill="currentColor" d="M4 8L0 4h8z"></path>
                </svg>
              </div>
            </div>


            <div class="u-form-group u-form-select u-form-group-3">
              <label for="select-5609" class="u-label u-label-3">TAG</label>
              <div class="u-form-select-wrapper">
                <select id="select-5609" name="select-1" class="u-grey-5 u-input u-input-rectangle u-input-3">
                  <option value="Disabilità cognitiva">Disabilità cognitiva</option>
                  <option value="Disabilità motoria">Disabilità motoria</option>
                  <option value="Disabilità psichica">Disabilità psichica</option>
                  <option value="Disabilità sensoriale">Disabilità sensoriale</option>
                </select>
                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="12" version="1" class="u-caret">
                  <path fill="currentColor" d="M4 8L0 4h8z"></path>
                </svg>
              </div>
            </div>


            <div class="u-align-right u-form-group u-form-submit">
              <a href="#" class="u-border-none u-btn u-btn-submit u-button-style u-custom-color-42 u-hover-custom-color-20 u-text-body-alt-color u-btn-1"><span class="u-icon u-icon-1"><svg class="u-svg-content" viewBox="0 0 52.966 52.966" x="0px" y="0px" style="width: 1em; height: 1em;">
                    <path d="M51.704,51.273L36.845,35.82c3.79-3.801,6.138-9.041,6.138-14.82c0-11.58-9.42-21-21-21s-21,9.42-21,21s9.42,21,21,21
	c5.083,0,9.748-1.817,13.384-4.832l14.895,15.491c0.196,0.205,0.458,0.307,0.721,0.307c0.25,0,0.499-0.093,0.693-0.279
	C52.074,52.304,52.086,51.671,51.704,51.273z M21.983,40c-10.477,0-19-8.523-19-19s8.523-19,19-19s19,8.523,19,19
	S32.459,40,21.983,40z"></path>
                  </svg><img></span> CERCA
              </a>
              <input type="submit" value="submit" class="u-form-control-hidden" wfd-invisible="true">
            </div>


            <!-- se il messaggio da errore ma per questo caso può anche non esserci -->
            <!-- <div class="u-form-send-message u-form-send-success" wfd-invisible="true">! Your message has been sent.</div>
            <div class="u-form-send-error u-form-send-message" wfd-invisible="true"> Unable to send your message. Please fix errors then try again. </div>
            <input type="hidden" value="" name="recaptchaResponse" wfd-invisible="true"> -->
          </form>
        </div>
      </div>
    </div>
  </div>
</section>