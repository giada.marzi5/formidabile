<!DOCTYPE html>
<html lang="it">

<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta charset="utf-8">
  <meta name="keywords" content="Progetto FormidAbile">
  <meta name="description" content="La piattaforma FormidAbile rende accessibili informazioni affidabili riguardanti disabilità e difficoltà fisiche, cognitive e psichiche dei giovani, e mette a disposizione materiali utili per la progettazione di attività formative ed educative inclusive.">
  <title>FormidAbile - Registrazione Utente</title>
  <meta name="generator" content="Nicepage 4.18.5, nicepage.com">
  <meta property="og:title" content="Piattaforma FormidAbile">
  <meta property="og:description" content="La piattaforma FormidAbile rende accessibili informazioni affidabili riguardanti disabilità e difficoltà fisiche, cognitive e psichiche dei giovani, e mette a disposizione materiali utili per la progettazione di attività formative ed educative inclusive.">
  <meta property="og:image" content="images/Immagine21.png">
  <meta property="og:url" content="formidabile.ch">
  <link rel="canonical" href="www.formidabile.ch">
  <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css?h=f1a8fe9e98944b9d682ec5c3efac8f17">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="/assets/css/styles.min.css?h=564c1c1d532286e87e9334866e3af3b2">
</head>

<body style="height: 1175.2px;">
  <!DOCTYPE html>
  <html style="font-size: 16px;" lang="it-CH">

  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="description" content="">
    <title>Login</title>
    <link rel="stylesheet" href="/assets/css/nicepage.css?h=d3ef2e118d136ba0c073be872af21893" media="screen">
    <link rel="stylesheet" href="Home.css" media="screen">
    <script class="u-script" type="text/javascript" src="jquery.js" "="" defer=""></script>
    <script class=" u-script" type="text/javascript" src="nicepage.js" "="" defer=""></script>
    <meta name=" generator" content="Nicepage 4.16.0, nicepage.com">
      <link id = "u-theme-google-font"
      rel = "stylesheet"
      href = "/assets/css/fonts.css?h=bdc0485dae6a022328f565ef478577a3" >
        <link id = "u-page-google-font"
      rel = "stylesheet"
      href = "Home-fonts.css" >

        <script type = "application/ld+json" > {
          "@context": "http://schema.org",
          "@type": "Organization",
          "name": "Piattaforma FormiDabile",
          "logo": "images/Immagine21.png",
          "sameAs": []
        }
    </script>
    <meta name="theme-color" content="#9b74ec">
    <meta property="og:title" content="Home">
    <meta property="og:description" content="">
    <meta property="og:type" content="website">
  </head>

  <body data-home-page="Home.html" data-home-page-title="Home" class="u-body u-xl-mode" data-lang="it">
    <header class="u-clearfix u-header u-white" id="sec-f943" data-animation-name="" data-animation-duration="0" data-animation-delay="0" data-animation-direction="">
      <div class="u-clearfix u-sheet u-valign-middle-lg u-valign-middle-xl u-sheet-1">
        <a href="Home.html" data-page-id="236022295" class="u-image u-logo u-image-1" data-image-width="1920" data-image-height="1725" title="Home">
          <img src="/assets/img/Immagine21.png?h=2602f3832a03913fd9c5a4e2b48f21aa" class="u-logo-image u-logo-image-1">
        </a>
        <a class="u-login u-text-body-color u-text-hover-custom-color-28 u-login-1" href="#" title="Login" target="_blank">Login</a>
        <nav class="u-menu u-menu-one-level u-offcanvas u-offcanvas-shift u-menu-1">
          <div class="menu-collapse" style="font-size: 1.125rem; letter-spacing: 0px; text-transform: uppercase; font-weight: 500;">
            <a class="u-button-style u-custom-active-border-color u-custom-active-color u-custom-border u-custom-border-color u-custom-borders u-custom-hover-border-color u-custom-hover-color u-custom-left-right-menu-spacing u-custom-padding-bottom u-custom-text-active-color u-custom-text-color u-custom-text-decoration u-custom-text-hover-color u-custom-top-bottom-menu-spacing u-nav-link" href="#" style="font-size: calc(1em + 16px); padding: 8px 18px; background-image: none;">
              <svg class="u-svg-link" viewBox="0 0 24 24">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-7d29"></use>
              </svg>
              <svg class="u-svg-content" version="1.1" id="svg-7d29" viewBox="0 0 16 16" x="0px" y="0px" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
                <g>
                  <rect y="1" width="16" height="2"></rect>
                  <rect y="7" width="16" height="2"></rect>
                  <rect y="13" width="16" height="2"></rect>
                </g>
              </svg>
            </a>
          </div>
          <div class="u-custom-menu u-nav-container">
            <ul class="u-nav u-spacing-0 u-unstyled u-nav-1">
              <li class="u-nav-item"><a class="u-border-active-palette-1-base u-border-hover-palette-1-light-1 u-border-no-left u-border-no-right u-border-no-top u-button-style u-nav-link u-text-active-custom-color-27 u-text-hover-custom-color-28" href="Home.html" style="padding: 2px 20px;">Home</a>
              </li>
              <li class="u-nav-item"><a class="u-border-active-palette-1-base u-border-hover-palette-1-light-1 u-border-no-left u-border-no-right u-border-no-top u-button-style u-nav-link u-text-active-custom-color-27 u-text-hover-custom-color-28" href="Chi-Siamo.html" style="padding: 2px 20px;">Chi Siamo</a>
              </li>
              <li class="u-nav-item"><a class="u-border-active-palette-1-base u-border-hover-palette-1-light-1 u-border-no-left u-border-no-right u-border-no-top u-button-style u-nav-link u-text-active-custom-color-27 u-text-hover-custom-color-28" href="Risorse.html" style="padding: 2px 20px;">Risorse</a>
              </li>
              <li class="u-nav-item"><a class="u-border-active-palette-1-base u-border-hover-palette-1-light-1 u-border-no-left u-border-no-right u-border-no-top u-button-style u-nav-link u-text-active-custom-color-27 u-text-hover-custom-color-28" href="CollaboraConNoi.html" style="padding: 2px 20px;">Collabora con noi</a>
              </li>
              <li class="u-nav-item"><a class="u-border-active-palette-1-base u-border-hover-palette-1-light-1 u-border-no-left u-border-no-right u-border-no-top u-button-style u-nav-link u-text-active-custom-color-27 u-text-hover-custom-color-28" href="Contatti.html" style="padding: 2px 20px;">Contatti</a>
              </li>
              <li class="u-nav-item"><a class="u-border-active-palette-1-base u-border-hover-palette-1-light-1 u-border-no-left u-border-no-right u-border-no-top u-button-style u-nav-link u-text-active-custom-color-27 u-text-hover-custom-color-28" style="padding: 2px 20px;">Prezzi</a>
              </li>
            </ul>
          </div>
          <div class="u-custom-menu u-nav-container-collapse">
            <div class="u-black u-container-style u-inner-container-layout u-opacity u-opacity-95 u-sidenav">
              <div class="u-inner-container-layout u-sidenav-overflow">
                <div class="u-menu-close"></div>
                <ul class="u-align-center u-nav u-popupmenu-items u-unstyled u-nav-2">
                  <li class="u-nav-item"><a class="u-button-style u-nav-link" href="Home.html">Home</a>
                  </li>
                  <li class="u-nav-item"><a class="u-button-style u-nav-link" href="Chi-Siamo.html">Chi Siamo</a>
                  </li>
                  <li class="u-nav-item"><a class="u-button-style u-nav-link" href="Risorse.html">Risorse</a>
                  </li>
                  <li class="u-nav-item"><a class="u-button-style u-nav-link" href="CollaboraConNoi.html">Collabora con
                      noi</a>
                  </li>
                  <li class="u-nav-item"><a class="u-button-style u-nav-link" href="Contatti.html">Contatti</a>
                  </li>
                  <li class="u-nav-item"><a class="u-button-style u-nav-link">Prezzi</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="u-black u-menu-overlay u-opacity u-opacity-70"></div>
          </div>
        </nav>
      </div>
    </header>


  </body>

  </html><!-- Start: VentasPro Login -->
  <div id="main" style="background: linear-gradient(-126deg, #ff9f02 0%, rgb(245,113,26) 24%, rgb(229,36,71) 44%, rgb(223,5,88) 62%, rgb(152,2,127) 78%, #3106b2);width: 1700px;height: 1124px;">
    <!-- Start: Pretty Registration Form -->
    <div class="row register-form" style="width: 1500px;height: 1103.4px;">
      <div class="col-md-8 offset-md-2">
        <form class="custom-form" style="border-style: initial;border-top-left-radius: 20px;border-top-right-radius: 20px;border-bottom-right-radius: 20px;border-bottom-left-radius: 20px;">
          <h1 style="font-family: Montserrat, sans-serif;font-size: 32px;border-color: rgba(227,37,110,0.6);">Registrazione Utente</h1>
          <div class="row form-group">
            <div class="col-sm-4 label-column"><label class="col-form-label" for="name-input-field" style="text-align: center;font-family: 'Open Sans', sans-serif;font-size: 16px;color: rgb(0,0,0);">Nome</label></div>
            <div class="col-sm-6 input-column"><input class="form-control" type="text" style="color: rgb(0,0,0);font-family: 'Open Sans', sans-serif;"></div>
          </div>
          <div class="row form-group">
            <div class="col-sm-4 label-column"><label class="col-form-label" for="name-input-field" style="font-family: 'Open Sans', sans-serif;font-size: 16px;color: rgb(0,0,0);">Cognome</label></div>
            <div class="col-sm-6 input-column"><input class="form-control" type="text" style="color: rgb(0,0,0);font-family: 'Open Sans', sans-serif;"></div>
          </div>
          <div class="row form-group">
            <div class="col-sm-4 label-column"><label class="col-form-label" for="dropdown-input-field" style="text-align: center;font-family: 'Open Sans', sans-serif;font-size: 16px;color: rgb(0,0,0);">Professione</label></div>
            <div class="col-sm-4 col-xxl-6 input-column"><input class="form-control" type="text" style="color: rgb(0,0,0);font-family: 'Open Sans', sans-serif;"></div>
          </div>
          <div class="row form-group">
            <div class="col-sm-4 label-column"><label class="col-form-label" for="name-input-field" style="text-align: center;font-family: 'Open Sans', sans-serif;font-size: 16px;color: rgb(0,0,0);">Indirizzo/Via</label></div>
            <div class="col-sm-6 input-column"><input class="form-control" type="text" style="color: rgb(0,0,0);font-family: 'Open Sans', sans-serif;"></div>
          </div>
          <div class="row form-group">
            <div class="col-sm-4 label-column"><label class="col-form-label" for="name-input-field" style="text-align: center;font-family: 'Open Sans', sans-serif;font-size: 16px;color: rgb(0,0,0);">Luogo</label></div>
            <div class="col-sm-6 input-column"><input class="form-control" type="text" style="color: rgb(0,0,0);font-family: 'Open Sans', sans-serif;"></div>
          </div>
          <div class="row form-group">
            <div class="col-sm-4 label-column"><label class="col-form-label" for="name-input-field" style="text-align: center;font-family: 'Open Sans', sans-serif;font-size: 16px;color: rgb(0,0,0);">Telefono</label></div>
            <div class="col-sm-6 input-column"><input class="form-control" type="text" style="color: rgb(0,0,0);font-family: 'Open Sans', sans-serif;"></div>
          </div>
          <div class="row form-group">
            <div class="col-sm-4 label-column"><label class="col-form-label" for="email-input-field" style="text-align: center;font-family: 'Open Sans', sans-serif;font-size: 16px;color: rgb(0,0,0);">Email </label></div>
            <div class="col-sm-6 input-column"><input class="form-control" type="email" style="color: rgb(0,0,0);font-family: 'Open Sans', sans-serif;"></div>
          </div>
          <div class="row form-group">
            <div class="col-sm-4 label-column"><label class="col-form-label" for="pawssword-input-field" style="text-align: center;font-family: 'Open Sans', sans-serif;font-size: 16px;color: rgb(0,0,0);">Password </label></div>
            <div class="col-sm-6 input-column"><input class="form-control" type="password" style="color: rgb(0,0,0);font-family: 'Open Sans', sans-serif;"></div>
          </div>
          <div class="row form-group">
            <div class="col-sm-4 label-column"><label class="col-form-label" for="repeat-pawssword-input-field" style="text-align: center;font-family: 'Open Sans', sans-serif;font-size: 16px;color: rgb(0,0,0);">Conferma Password </label></div>
            <div class="col-sm-6 input-column"><input class="form-control" type="password" style="color: rgb(0,0,0);font-family: 'Open Sans', sans-serif;"></div>
          </div>
          <div class="form-check"><input class="form-check-input" type="checkbox" id="formCheck-1" style="margin: 7.5px 0px 0px 242px;padding: 0px;"><label class="form-check-label" for="formCheck-1" style="margin: 7px 0px 0px -134px;padding: 0px;font-family: 'Open Sans', sans-serif;font-size: 16px;">Ho letto e accetto i termini di condizione</label></div><button class="btn btn-light submit-button" type="button" style="color: rgb(255, 255, 255);font-family: 'Open Sans', sans-serif;background: #e3256e;">Registrati</button>
        </form>
      </div>
    </div><!-- End: Pretty Registration Form -->
    <!DOCTYPE html>
    <html style="font-size: 16px;" lang="it-CH">

    <head>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta charset="utf-8">
      <meta name="description" content="">
      <title>Login</title>
      <link rel="stylesheet" href="/assets/css/nicepage.css?h=d3ef2e118d136ba0c073be872af21893" media="screen">
      <link rel="stylesheet" href="Home.css" media="screen">
      <script class="u-script" type="text/javascript" src="jquery.js" "="" defer=""></script>
    <script class=" u-script" type="text/javascript" src="nicepage.js" "="" defer=""></script>
    <meta name=" generator" content="Nicepage 4.16.0, nicepage.com">
        < link id = "u-theme-google-font"
        rel = "stylesheet"
        href = "/assets/css/fonts.css?h=bdc0485dae6a022328f565ef478577a3" >
          <
          link id = "u-page-google-font"
        rel = "stylesheet"
        href = "Home-fonts.css" >

          <
          script type = "application/ld+json" > {
            "@context": "http://schema.org",
            "@type": "Organization",
            "name": "Piattaforma FormiDabile",
            "logo": "images/Immagine21.png",
            "sameAs": []
          }
      </script>
      <meta name="theme-color" content="#9b74ec">
      <meta property="og:title" content="Home">
      <meta property="og:description" content="">
      <meta property="og:type" content="website">
    </head>

    <body data-home-page="Home.html" data-home-page-title="Home" class="u-body u-xl-mode" data-lang="it">
      <footer class="u-clearfix u-footer u-white" id="sec-530e">
        <div class="u-clearfix u-sheet u-sheet-1">
          <div class="u-align-left u-social-icons u-spacing-20 u-social-icons-1">
            <a class="u-social-url" title="facebook" target="_blank" href=""><span class="u-icon u-social-facebook u-social-icon u-icon-1"><svg class="u-svg-link" preserveAspectRatio="xMidYMin slice" viewBox="0 0 112 112">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-f425"></use>
                </svg><svg class="u-svg-content" viewBox="0 0 112 112" x="0" y="0" id="svg-f425">
                  <circle fill="currentColor" cx="56.1" cy="56.1" r="55"></circle>
                  <path fill="#FFFFFF" d="M73.5,31.6h-9.1c-1.4,0-3.6,0.8-3.6,3.9v8.5h12.6L72,58.3H60.8v40.8H43.9V58.3h-8V43.9h8v-9.2
            c0-6.7,3.1-17,17-17h12.5v13.9H73.5z"></path>
                </svg></span>
            </a>
            <a class="u-social-url" title="instagram" target="_blank" href="https://www.instagram.com/formidabile.ch/"><span class="u-icon u-social-icon u-social-instagram u-icon-2"><svg class="u-svg-link" preserveAspectRatio="xMidYMin slice" viewBox="0 0 112 112">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-eba1"></use>
                </svg><svg class="u-svg-content" viewBox="0 0 112 112" x="0" y="0" id="svg-eba1">
                  <circle fill="currentColor" cx="56.1" cy="56.1" r="55"></circle>
                  <path fill="#FFFFFF" d="M55.9,38.2c-9.9,0-17.9,8-17.9,17.9C38,66,46,74,55.9,74c9.9,0,17.9-8,17.9-17.9C73.8,46.2,65.8,38.2,55.9,38.2
            z M55.9,66.4c-5.7,0-10.3-4.6-10.3-10.3c-0.1-5.7,4.6-10.3,10.3-10.3c5.7,0,10.3,4.6,10.3,10.3C66.2,61.8,61.6,66.4,55.9,66.4z"></path>
                  <path fill="#FFFFFF" d="M74.3,33.5c-2.3,0-4.2,1.9-4.2,4.2s1.9,4.2,4.2,4.2s4.2-1.9,4.2-4.2S76.6,33.5,74.3,33.5z"></path>
                  <path fill="#FFFFFF" d="M73.1,21.3H38.6c-9.7,0-17.5,7.9-17.5,17.5v34.5c0,9.7,7.9,17.6,17.5,17.6h34.5c9.7,0,17.5-7.9,17.5-17.5V38.8
            C90.6,29.1,82.7,21.3,73.1,21.3z M83,73.3c0,5.5-4.5,9.9-9.9,9.9H38.6c-5.5,0-9.9-4.5-9.9-9.9V38.8c0-5.5,4.5-9.9,9.9-9.9h34.5
            c5.5,0,9.9,4.5,9.9,9.9V73.3z"></path>
                </svg></span>
            </a>
            <a class="u-social-url" title="linkedin" target="_blank" href=""><span class="u-icon u-social-icon u-social-linkedin u-icon-3"><svg class="u-svg-link" preserveAspectRatio="xMidYMin slice" viewBox="0 0 112 112">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-f491"></use>
                </svg><svg class="u-svg-content" viewBox="0 0 112 112" x="0" y="0" id="svg-f491">
                  <circle fill="currentColor" cx="56.1" cy="56.1" r="55"></circle>
                  <path fill="#FFFFFF" d="M41.3,83.7H27.9V43.4h13.4V83.7z M34.6,37.9L34.6,37.9c-4.6,0-7.5-3.1-7.5-7c0-4,3-7,7.6-7s7.4,3,7.5,7
            C42.2,34.8,39.2,37.9,34.6,37.9z M89.6,83.7H76.2V62.2c0-5.4-1.9-9.1-6.8-9.1c-3.7,0-5.9,2.5-6.9,4.9c-0.4,0.9-0.4,2.1-0.4,3.3v22.5
            H48.7c0,0,0.2-36.5,0-40.3h13.4v5.7c1.8-2.7,5-6.7,12.1-6.7c8.8,0,15.4,5.8,15.4,18.1V83.7z"></path>
                </svg></span>
            </a>
          </div>
          <div class="u-border-1 u-border-black u-expanded-width u-line u-line-horizontal u-opacity u-opacity-50 u-line-1"></div>
          <p class="u-align-center u-text u-text-1">Copyright&copy;; Formidabile 2022 | Web Design by Giada Marzi
          </p>
        </div>
      </footer>

    </body>

    </html>
  </div><!-- End: VentasPro Login -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
  <script src="/assets/js/script.min.js?h=c70aa116a8efa441fa49506aef31d8f5"></script>
</body>

</html>