<?php

require_once('FormProcessor.php');

$form = array(
    'subject' => 'Nuovi documenti caricati.',
    'email_message' => 'Sono stati inseriti dei nuovi documenti, da confermare.',
    'success_redirect' => '',
    'sendIpAddress' => false,
    'email' => array(
    'from' => '',
    'to' => ''
    ),
    'fields' => array(
    'disabilità' => array(
    'order' => 1,
    'type' => 'email',
    'label' => 'Disabilità',
    'required' => true,
    'errors' => array(
    'required' => 'Field \'Disabilità\' is required.'
    )
    ),
    'descrizione' => array(
    'order' => 2,
    'type' => 'string',
    'label' => 'Descrizione',
    'required' => true,
    'errors' => array(
    'required' => 'Field \'Descrizione\' is required.'
    )
    ),
    'text' => array(
    'order' => 3,
    'type' => 'string',
    'label' => 'Aggiungi tag',
    'required' => false,
    'errors' => array(
    'required' => 'Field \'Aggiungi tag\' is required.'
    )
    ),
    )
    );

    $processor = new FormProcessor('');
    $processor->process($form);

    ?>