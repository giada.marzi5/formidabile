<?php

require_once('FormProcessor.php');

$form = array(
    'subject' => 'Nuova Iscrizione alla newsletter',
    'email_message' => 'Hai una nuova richiesta',
    'success_redirect' => '',
    'sendIpAddress' => false,
    'email' => array(
    'from' => '',
    'to' => ''
    ),
    'fields' => array(
    'email' => array(
    'order' => 1,
    'type' => 'email',
    'label' => 'Email',
    'required' => true,
    'errors' => array(
    'required' => 'Field \'Email\' is required.'
    )
    ),
    )
    );

    $processor = new FormProcessor('');
    $processor->process($form);

    ?>