<?php

require_once('FormProcessor.php');

$form = array(
    'subject' => 'Nuova richiesta del modulo',
    'email_message' => 'Hai una nuova richiesta',
    'success_redirect' => '',
    'sendIpAddress' => false,
    'email' => array(
    'from' => '',
    'to' => ''
    ),
    'fields' => array(
    'email' => array(
    'order' => 1,
    'type' => 'email',
    'label' => 'Email',
    'required' => true,
    'errors' => array(
    'required' => 'Field \'Email\' is required.'
    )
    ),
    'name' => array(
    'order' => 2,
    'type' => 'string',
    'label' => 'Nome',
    'required' => true,
    'errors' => array(
    'required' => 'Field \'Nome\' is required.'
    )
    ),
    'phone' => array(
    'order' => 3,
    'type' => 'tel',
    'label' => 'Telefono',
    'required' => false,
    'errors' => array(
    'required' => 'Field \'Telefono\' is required.'
    )
    ),
    'message-1' => array(
    'order' => 4,
    'type' => 'string',
    'label' => 'Messaggio',
    'required' => true,
    'errors' => array(
    'required' => 'Field \'Messaggio\' is required.'
    )
    ),
    )
    );

    $processor = new FormProcessor('');
    $processor->process($form);

    ?>