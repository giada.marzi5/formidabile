<?php

namespace App\Models;


use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\File;
use Spatie\YamlFrontMatter\YamlFrontMatter;
use Symfony\Component\Yaml\Yaml;


class Post
{

    public $title;

    public $excerpt;

    public $date;

    public $body;

    public $slug;

    public function __construct($title, $excerpt, $date, $body, $slug)
    {
        $this->title = $title;
        $this->excerpt = $excerpt;
        $this->date = $date;
        $this->body = $body;
        $this->slug = $slug;
    }


    public static function all()
    {


        //collect serve per creare una collezione che ti da un valore
        //map serve per applicare una funzione di callback agli elementi (dell array con array_map)    
        return collect(File::files(resource_path("posts")))
            ->map(fn ($file) => YamlFrontMatter::parseFile($file))
            ->map(fn ($document) => new Post(
                $document->title,
                $document->excerpt,
                $document->date,
                $document->body(),
                $document->slug
            ))
            ->sortByDesc('date');
    }


    public static function find($slug)
    {
        //per tutti i post, trova quello con il slug che l'utente ha richiesto

        // $posts = static::all();

        return  static::all()->firstWhere('slug', $slug);



        //     //Stesso di quello che trovo sotto web.php commentato ma modificato per fungere da funzione
        //     base_path();
        //     if (!file_exists($path = resource_path("posts/{$slug}.html"))) {
        //         throw new ModelNotFoundException();
        //     }

        //     return cache()->remember("posts.{$slug}", 1200, fn () => file_get_contents($path));
    }

    public static function findOrFail($slug)
    {


        $post =  static::find($slug);

        if (!$post) {
            throw new ModelNotFoundException();
        }

        return $post;
    }
}
