<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
     //fillable il campo è visto da tutti

    //  protected $guarded = []; // sono i campi che vengono ingoranti nella insert

     protected $with = ['category','author'];

     public function scopeFilter($query, array $filters)
     {
         //per il testo ricerca
         $query->when(
             $filters['search'] ?? false,
             fn ($query, $search) =>
             //qui metto che il post che l'utente cerca deve essere come uno dei post nella pagina
             //questo lo faccio con il like
             $query->where(
                 fn ($query) =>
                 $query->where('title', 'like', '%' . $search . '%')
                     ->Orwhere('body', 'like', '%' . $search . '%')
             )
         );
 
         $query->when(
             $filters['category'] ?? false,
             fn ($query, $category) =>
 
             // $query->whereExists(fn ($query) =>
             // $query->from('categories')
             //     ->whereColumn('categories.id', 'posts.category_id')
             //     ->where('categories.slug', $category))
 
             // la semplifichiamo con questo whereHas
             $query->whereHas('category', fn ($query) => $query->where('slug', $category))
 
         );
         $query->when(
             $filters['author'] ?? false,
             fn ($query, $author) =>
             $query->whereHas('author', fn ($query) => $query->where('username', $author))
 
         );
     }
 
     public function category(){
         //prima eloquent relationship
         return $this->belongsTo(Category::class);
     }
     // public function user(){
     //     //prima eloquent relationship
     //     return $this->belongsTo(User::class);
     // }
     public function author(){
         //prima eloquent relationship
         return $this->belongsTo(User::class, 'user_id');
     }
}
